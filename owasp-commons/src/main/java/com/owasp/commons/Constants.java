package com.owasp.commons;

/**
 * Clase para definir constantes comunes a los distintos modulos
 * @author govx
 *
 */
public class Constants {

	public static final String SISTEMA = "OWASP-SPRING";
	
	public static final String SECURITY_REALM = "OWASP_REALM";
	
	private static final String API_VERSION = "v1";
	
	public static final String API_PATH_V1= "/api/" + API_VERSION + "/";
	
	public static final int PERSON_NOT_FOUND_CODE = 101;
	public static final String PERSON_NOT_FOUND_MSG = "person.not.found";
	
	public static final int INVALID_PERSON_CODE = 102;
	public static final String INVALID_PERSON_MSG = "invalid.person";

	public static final int INVALID_MESSAGE_CODE = 301;
	public static final String INVALID_MESSAGE_MSG = "invalid.message";

}
