package com.owasp.commons.exception;

import com.owasp.commons.Constants;

public class InvalidPersonException extends BusinessException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPersonException() {
		super(Constants.INVALID_PERSON_CODE, Constants.INVALID_PERSON_MSG);
	}

	public InvalidPersonException( String detail ) {
		super(Constants.INVALID_PERSON_CODE, Constants.INVALID_PERSON_MSG);
		super.setDetail(detail);
	}

}
