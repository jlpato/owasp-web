package com.owasp.commons.exception;

import com.owasp.commons.Constants;

public class PersonNotFound extends BusinessException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersonNotFound() {
		super(Constants.PERSON_NOT_FOUND_CODE, Constants.PERSON_NOT_FOUND_MSG);
	}

}
