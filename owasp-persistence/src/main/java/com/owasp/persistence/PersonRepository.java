package com.owasp.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import com.owasp.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long>{


}
