package com.owasp.service;

import java.util.List;

import com.owasp.commons.exception.BusinessException;
import com.owasp.model.Person;

public interface PersonService {

	public Person create(Person person);
	public Person delete(Long id) throws BusinessException;
	public List<Person> findAll();
	public Person update(Person person) throws BusinessException;
	public Person findById(Long id);
	public Person findByUsername(String username);

}
