package com.owasp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.owasp.service.PersonService;
import com.owasp.commons.exception.PersonNotFound;
import com.owasp.commons.exception.BusinessException;
import com.owasp.model.Person;
import com.owasp.persistence.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);
	
	@Resource
	private PersonRepository personRepository;

	@Transactional
	public Person create(Person person) {
		List<Person> all = personRepository.findAll();
		for(Person p: all){
			LOGGER.debug(p.toString());
		}
		Person createdPerson = personRepository.saveAndFlush(person);
		return createdPerson;
	}

	@Transactional(rollbackFor=PersonNotFound.class)
	public Person delete(Long id) throws BusinessException {
		Person deletedPerson = personRepository.findOne(id);
        if (deletedPerson == null){
        	throw new PersonNotFound();
        }
        personRepository.delete(deletedPerson);
        return deletedPerson;
	}

	public List<Person> findAll() {
		return personRepository.findAll();
	}

	@Transactional(rollbackFor=PersonNotFound.class)
	public Person update(Person person) throws BusinessException {
		Person updatedPerson = personRepository.findOne(person.getId());
        if (updatedPerson == null){
        	throw new PersonNotFound();
        }
        updatedPerson.setName(person.getName());
        updatedPerson.setLocation(person.getLocation());
        return updatedPerson;
	}

	@Transactional
	public Person findById(Long id) {
		return personRepository.findOne(id);
	}

	public Person findByUsername(String username) {
		
		Person person = new Person();                         
		person.setName(username);                          
		Example<Person> example = Example.of(person);
		
		Person found = personRepository.findOne(example);
		return found;
		
	}

}
