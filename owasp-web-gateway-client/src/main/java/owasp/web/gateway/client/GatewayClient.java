package owasp.web.gateway.client;

import java.net.URLEncoder;
import java.util.Base64;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.owasp.commons.dto.PersonDTO;

/**
 * Gateway client!
 *
 */
public class GatewayClient {
	
	String endpoint = "";
	
	public GatewayClient( String endpoint ) {
		this.endpoint = endpoint;
	}
	
	public String personCall( String user, String token, OperationType operation, String requestJson ){

		RestTemplate restTemplate = new RestTemplate();
		String url = endpoint + operation.getPath();
		
		if( OperationType.PERSON_FIND.equals(operation) ){
			url = url + "/" + requestJson;
			requestJson = "";
		}
		
		String plainCredentials = user + ":" + token;
		String base64Credentials = new String(Base64.getEncoder().encodeToString(plainCredentials.getBytes()));
		System.out.println( base64Credentials );
		
		CsrfTokenRepository csrfTokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse();
		CsrfToken csrfToken = csrfTokenRepository.generateToken(null);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Basic " + base64Credentials);//"Basic ASFASFSDFSDFSDF"
		headers.add(csrfToken.getHeaderName(), csrfToken.getToken());
		headers.add("Cookie", "XSRF-TOKEN=" + csrfToken.getToken());

		HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);
		String answer = restTemplate.postForObject(url, entity, String.class);
		return answer;
		
	}
	
	public static void testCreate() {
		GatewayClient gatewayClient = new GatewayClient( "http://localhost:8080/gateway/" );
		String requestJson4Create = "{\"location\":\"Islandia\",\"name\":\"Roger\"}";
		String rst = gatewayClient.personCall("priya", "priya", OperationType.PERSON_CREATE, requestJson4Create);
		System.out.println( "DONE->" + rst );
		PersonDTO person = (new Gson()).fromJson(rst, PersonDTO.class);
		System.out.println( "DTO->" + person.getName() );
	}

	public static void testUpdate() {
		GatewayClient gatewayClient = new GatewayClient( "http://localhost:8080/gateway/" );
		String requestJson4Update = "{\"id\":1, \"location\":\"Rusia\", \"name\":\"Alex\"}";
		String rst = gatewayClient.personCall("priya", "priya", OperationType.PERSON_UPDATE, requestJson4Update);
		System.out.println( "DONE->" + rst );
		PersonDTO person = (new Gson()).fromJson(rst, PersonDTO.class);
		System.out.println( "DTO->" + person.getName() );
	}
	
	public static void testFind() {
		GatewayClient gatewayClient = new GatewayClient( "http://localhost:8080/gateway/" );
		String requestJson4Update = "1";
		String rst = gatewayClient.personCall("priya", "priya", OperationType.PERSON_FIND, requestJson4Update);
		System.out.println( "DONE->" + rst );
		PersonDTO person = (new Gson()).fromJson(rst, PersonDTO.class);
		System.out.println( "DTO->" + person.getName() );
	}

	public static void testLongPath() {

		RestTemplate restTemplate = new RestTemplate();
		
		String fecha = URLEncoder.encode("2017-04-19T22:08:28");
		String formReg = URLEncoder.encode("[A-Z&Ñ]%7B3,4%7D[0-9]%7B2%7D(0[1-9]%7C1[012])(0[1-9]%7C[12][0-9]%7C3[01])[A-Z0-9]%7B2%7D[0-9A]");
		String fechaTimbrado = URLEncoder.encode("2013-04-2000:47:12");

//		String fecha = "2017-04-19T22:08:28";
//		String formReg = "[A-Z&Ñ]%7B3,4%7D[0-9]%7B2%7D(0[1-9]%7C1[012])(0[1-9]%7C[12][0-9]%7C3[01])[A-Z0-9]%7B2%7D[0-9A]";
//		String fechaTimbrado = "2013-04-2000:47:12";

		String url =
				"http://localhost:8080/gateway/person/" +
				"user/testUser/pass/1234/hashCFDI/6a11reg1e6a1g6reg1a/rfc/MARH6010124I6/fecha/" + fecha +
				"/pedimento/10++20++1234++2000040/uuid/f116d525-a543-4955-ad56-7cb273bf1074/unidad/A91/cp/01080/cvlp/" +
				"01010101/formPay/0/methodPay/PUE/monedaCvl/AED/paisCvl/MEX/formReg/" + formReg +
				"/factor/Tasa/comprobante/E/patente/4001/regimen/601/relacion/06/usoCfdi/G01/rfcReceptor/RTAFC5T51505/fechaTimbrado/" +
				fechaTimbrado + "/monto/1583/xml/cadenaxml/request/EMPTY/serie/00001000000203393837/folio/EMPTY/complemento/EMPTY/codigo/4055191510/tiempo/2/tipoSeleccion/1/numeroCertificado/0509509099";
		
		System.out.println( url  );
		
		String plainCredentials = "priya:priya";
		String base64Credentials = new String(Base64.getEncoder().encodeToString(plainCredentials.getBytes()));
		System.out.println( base64Credentials );
		
		CsrfTokenRepository csrfTokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse();
		CsrfToken csrfToken = csrfTokenRepository.generateToken(null);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", "Basic " + base64Credentials);//"Basic ASFASFSDFSDFSDF"
		headers.add(csrfToken.getHeaderName(), csrfToken.getToken());
		headers.add("Cookie", "XSRF-TOKEN=" + csrfToken.getToken());

		HttpEntity<String> entity = new HttpEntity<String>("",headers);
		String answer = restTemplate.postForObject(url, entity, String.class);
		
		System.out.println( "DONE->" + answer );
//		ResultadoDatapower person = (new Gson()).fromJson(answer, ResultadoDatapower.class);
//		System.out.println( "DTO->" + person.getName() );
	}

	public static void main(String[] args) {
//		testCreate();
//		testUpdate();
//		testFind();
		testLongPath();
	}
}
