package com.owasp.web.gateway.component;

import com.owasp.commons.exception.BusinessException;
import com.owasp.model.Person;

public interface IPersonPlatform {
	
	public Person getPersonDetail(Integer id) throws BusinessException;

	public Person create(Person person) throws BusinessException;

	public Person update(Person person) throws BusinessException;

	public Person getPersonDetailCached(Integer id) throws BusinessException;

}
