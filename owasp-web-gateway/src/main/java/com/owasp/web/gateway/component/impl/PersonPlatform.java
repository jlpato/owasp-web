package com.owasp.web.gateway.component.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.owasp.web.gateway.component.IPersonPlatform;
import com.owasp.commons.exception.InvalidPersonException;
import com.owasp.commons.exception.BusinessException;
import com.owasp.model.Person;
import com.owasp.web.gateway.repository.PersonRepository;

@Component
public class PersonPlatform implements IPersonPlatform{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonPlatform.class);
	
	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public Person getPersonDetail(Integer id) throws BusinessException{
		try{
			return personRepository.getPerson(id);
		}catch( DataAccessException e ){
			throw new InvalidPersonException( e.getMessage() );
		}
	}

	@Override
	public Person create(final Person person) throws BusinessException{
		try{
			return personRepository.create(person);
		}catch( DataAccessException e ){
			throw new InvalidPersonException( e.getMessage() );
		}
	}
	
	@Override
	@PreAuthorize("hasRole(ADMIN)")
	public Person update(final Person person) throws BusinessException{
		return personRepository.update( person );
	}

	@Override
	@Cacheable("personDetailCached")
//	@CacheEvict("personDetailCached")
	public Person getPersonDetailCached(Integer id) throws BusinessException {
		try{
			LOGGER.debug("Executing getPersonDetailCached()");
			return personRepository.getPerson(id);
		}catch( DataAccessException e ){
			throw new InvalidPersonException( e.getMessage() );
		}
	}

	@CacheEvict("personDetailCached")
	public Person evictPersonCached(Integer id) throws BusinessException {
		try{
			LOGGER.debug("Executing getPersonDetailCached()");
			return personRepository.getPerson(id);
		}catch( DataAccessException e ){
			throw new InvalidPersonException( e.getMessage() );
		}
	}

}
