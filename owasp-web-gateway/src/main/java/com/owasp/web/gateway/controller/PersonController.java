package com.owasp.web.gateway.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.owasp.commons.exception.BusinessException;
import com.owasp.model.Person;
import com.owasp.web.gateway.component.IPersonPlatform;
import com.owasp.web.gateway.logical.PersonLogical;

@RestController
@RequestMapping("/person")
public class PersonController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

	@Autowired
	private IPersonPlatform personPlatform;
	
	private Gson gson = new Gson();

//	http://localhost:8080/gateway/person/find/2323
	@RequestMapping("/find/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseEntity<Person> getPersonDetail( @PathVariable Integer id ) {
		LOGGER.debug("find");
		Person person = null;
		try {
			person = personPlatform.getPersonDetail(id);
			return new ResponseEntity<Person>(person, HttpStatus.OK);
		} catch (BusinessException e) {
			LOGGER.error( e.getDetail(), e );
		}
		return new ResponseEntity<Person>(person, HttpStatus.BAD_REQUEST);
	}

	@RequestMapping( path = "/create", method = { RequestMethod.POST } )
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public ResponseEntity<Person> createPerson(final HttpEntity<String> entity) {
		LOGGER.debug("create");
		try {
	    	Person person = gson.fromJson( entity.getBody(), Person.class );
			PersonLogical.validateCreate( person );
	    	person = personPlatform.create( person );
	    	return new ResponseEntity<Person>(person, HttpStatus.OK);
		} catch (BusinessException e) {
			LOGGER.error( e.getDetail(), e );
		}
		return new ResponseEntity<Person>(new Person(), HttpStatus.BAD_REQUEST);
	}

	
	@RequestMapping( path = "/update", method = { RequestMethod.POST } )
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public ResponseEntity<Person> updatePerson(final HttpEntity<String> entity) {
		LOGGER.debug("update");
		try {
	    	Person person = gson.fromJson( entity.getBody(), Person.class );
			PersonLogical.validateUpdate( person );
	    	person = personPlatform.update( person );
	    	return new ResponseEntity<Person>(person, HttpStatus.OK);
		} catch (BusinessException e) {
			LOGGER.error( e.getDetail(), e );
		}
		return new ResponseEntity<Person>(new Person(), HttpStatus.BAD_REQUEST);
    }

	
//	http://localhost:8080/gateway/person/findCached/2323
	@RequestMapping("/findCached/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseEntity<Person> getPersonDetailCached( @PathVariable Integer id ) {
		LOGGER.debug("findCached");
		Person person = null;
		try {
			person = personPlatform.getPersonDetailCached(id);
			return new ResponseEntity<Person>(person, HttpStatus.OK);
		} catch (BusinessException e) {
			LOGGER.error( e.getDetail(), e );
		}
		return new ResponseEntity<Person>(person, HttpStatus.BAD_REQUEST);
	}

}
