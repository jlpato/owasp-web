package com.owasp.web.gateway.logical;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.owasp.commons.exception.InvalidPersonException;
import com.owasp.commons.exception.BusinessException;
import com.owasp.model.Person;

public class PersonLogical {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonLogical.class);

	public static void validateCreate( Person person ) throws BusinessException{
		if( person == null ){
			throw new InvalidPersonException();
		}
		if( StringUtils.isEmpty( person.getName() ) ){
			throw new InvalidPersonException("El NOMBRE no es valido");
		}
		if( StringUtils.isEmpty( person.getLocation() ) ){
			throw new InvalidPersonException("La localizacion no es valida");
		}
		LOGGER.debug( person.toString() );
	}

	public static void validateUpdate(Person person) throws BusinessException{
		if( person == null ){
			throw new InvalidPersonException();
		}
		if( person.getId() <= 0 ){
			throw new InvalidPersonException("El ID no es valido");
		}
		if( StringUtils.isEmpty( person.getName() ) ){
			throw new InvalidPersonException("El NOMBRE no es valido");
		}
		if( StringUtils.isEmpty( person.getLocation() ) ){
			throw new InvalidPersonException("La localizacion no es valida");
		}
		LOGGER.debug( person.toString() );
	}
}
