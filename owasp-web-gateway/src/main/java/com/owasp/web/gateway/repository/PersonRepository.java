package com.owasp.web.gateway.repository;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.owasp.web.gateway.mapper.PersonMapper;
import com.owasp.model.Person;

@Repository
public class PersonRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonRepository.class);
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	
	public Person create(final Person person) {
		LOGGER.debug("PersonRepository:::create( " + person.toString() + " )");
		long primaryKey = jdbcTemplate.queryForObject( "(select max(id) + 1 from person)", Long.class );
		this.jdbcTemplate.update(
				"insert into person ( id, location, name) values ( ?, ?, ? )",
				primaryKey,
				person.getLocation(),
				person.getName());
		person.setId( primaryKey );
		return person;
	}
	
	public Person getPerson( Integer id ){
		LOGGER.debug("PersonRepository:::getPerson( " + id + " )");
		Person person = (Person)this.jdbcTemplate.queryForObject(
				"select id, name, location from person where id = ?",
				new Object[]{ id },
				new PersonMapper());
		LOGGER.debug("PersonRepository:::person( " + person.toString() + " )");
		return person;
	}

	public Person update(Person person) {
		LOGGER.debug("PersonRepository:::update( " + person.toString() + " )");
		this.jdbcTemplate.update(
				"update person set location = ?, name = ? where id = ?",
				person.getLocation(),
				person.getName(),
				person.getId());
		return person;
	}

}
