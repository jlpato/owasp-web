package com.owasp.web.gateway.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration 
@ComponentScan({
	"com.owasp.web.gateway.component",
	"com.owasp.web.gateway.controller",
	"com.owasp.web.gateway.repository"
	})
@EnableWebMvc
@Import({
	PersistenceConfig.class,
	CachingConfig.class,
	SecurityConfig.class
	})
public class AppConfig extends AbstractSecurityWebApplicationInitializer{

	public AppConfig() {
	}
}
