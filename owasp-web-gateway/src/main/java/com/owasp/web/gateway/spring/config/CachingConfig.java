package com.owasp.web.gateway.spring.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableCaching
public class CachingConfig {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CachingConfig.class);

	@Bean
    public CacheManager cacheManager() {
		LOGGER.debug( "CachingConfig::cacheManager" );
		return new EhCacheCacheManager(ehCacheCacheManager().getObject());
    }
	
	@Bean
	public EhCacheManagerFactoryBean ehCacheCacheManager() {
		LOGGER.debug( "CachingConfig::ehCacheCacheManager" );
		EhCacheManagerFactoryBean cmfb = new EhCacheManagerFactoryBean();
		cmfb.setConfigLocation(new ClassPathResource("ehcache.xml"));
		cmfb.setShared(true);
		return cmfb;
	}
	
}
