package com.owasp.web.gateway.spring.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

//@Profile("development")
@Configuration
public class PersistenceConfig {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceConfig.class);
	
	@Bean
	public DataSource dataSource() {
		LOGGER.debug("PersistenceConfig:::dataSource()");
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder
			.setType(EmbeddedDatabaseType.HSQL)
			.addScript("db/create-db.sql")
			.addScript("db/insert-data.sql")
			.build();
		return db;
	}
	
	@Bean
	public JdbcTemplate getJdbcTemplate( DataSource dataSource ) {
		LOGGER.debug("PersistenceConfig:::getJdbcTemplate()->dataSource:" + dataSource);
		return new JdbcTemplate(dataSource);
	}
	
}
