package com.owasp.web.pacrdd.spring.config;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class ApplicationListenerComponent
implements ApplicationListener<ApplicationEvent>, HttpSessionListener
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationListenerComponent.class);
	
	String msgPrefix = "\n\n.........(0 0)\n.---oOO-- (_)-----.\n╔═════════════════╗\n";
	String msgSuffix = "\n╚═════════════════╝\n'--------------oOO\n........|__|__|\n........ || ||\n....... ooO Ooo\n\n\n";
	private String msg( String msg ){
		return msgPrefix + msg + msgSuffix;
	};
	
	@Override
	public void onApplicationEvent(ApplicationEvent appEvent) {
		LOGGER.debug("ALC::onApplicationEvent::caching event:: " + appEvent);
		LOGGER.debug( msg(""+appEvent.getClass()) );
        if(appEvent instanceof AuthenticationSuccessEvent || appEvent instanceof  InteractiveAuthenticationSuccessEvent )
        {
            // Authenticacion success
        	LOGGER.debug("ALC::onApplicationEvent::AuthenticationSuccessEvent");
        	AuthenticationSuccessEvent event = (AuthenticationSuccessEvent) appEvent;
        	UserDetails userDetails = (UserDetails) event.getAuthentication().getPrincipal();
        	String msg = appEvent.getClass() + " -> " + userDetails.getUsername();
        	LOGGER.debug( msg( msg ) );
        	// TODO guardar bitacora
        } 
        else if (appEvent instanceof AbstractAuthenticationFailureEvent)
        {
            // Authentication failure
        	LOGGER.debug("ALC::onApplicationEvent::AbstractAuthenticationFailureEvent");
        	// TODO guardar bitacora
        }		
        else if (appEvent instanceof SessionDestroyedEvent)
        {
            // Logout from session ( even timeout session )
        	LOGGER.debug("ALC::onApplicationEvent::SessionDestroyedEvent");
        	// TODO guardar bitacora
        }
        
	}

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		// Create session ( even timeout session )
    	LOGGER.debug("ALC::HttpSessionListener::sessionCreated");
    	LOGGER.debug( msg(event.getClass() + "->sessionCreated") );
    	event.getSession().setMaxInactiveInterval(10*60);
    	// TODO guardar bitacora
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		// destroy session ( even timeout session )
    	LOGGER.debug("ALC::HttpSessionListener::sessionDestroyed");
    	String msg = event.getClass() + " -> sessionDestroyed -> ";
    	LOGGER.debug( msg( msg ) );
    	// TODO guardar bitacora	
	}

}
