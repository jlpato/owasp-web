<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>2013 - 2017</h1>

		<hr/>
		<h2>&iquest;Qu&eacute; ha cambiado de 2013 a 2017?</h2>
		<p>Nuevos riesgos, respaldados en datos:</p>
		<ul>
			<li>A4:2017 - Entidades Externas XML (XXE)</li>
		</ul>

		<p>Nuevos riesgos, respaldados por la comunidad</p>
		<ul>
			<li>A8:2017 - Deserializaci&oacute;n Insegura</li>
			<li>A10:2017 - Registro y Monitoreo Insuficientes</li>
		</ul>

		<p>Fusionados</p>
		<ul>
			<li>A5:2017 - P&eacute;rdida de Control de Acceso
				<ul>
					<li>A4 - Referencia Directa Insegura a Objetos</li>
					<li>A7 - Ausencia de Control de Acceso a las Funciones</li>
				</ul>
			</li>
		</ul>
		
		<p>Retirados</p>
		<ul>
			<li>A8 - Falsificaci&oacute;n de Peticiones en Sitios Cruzados (CSRF)
				<ul>
					<li>La mayor&iacute;a de frameworks incluyen defensas contra CSRF.</li>
					<li>S&oacute;lo se encontr&oacute; en el 5% de las aplicaciones.</li>
				</ul>
			</li>
		</ul>
		<ul>
			<li>A10 - Redirecciones y reenv&iacute;os no validados
				<ul>
					<li>Se encuentra en el 8% de las aplicaciones.</li>
					<li>Fu&eacute; superado por XXE.</li>
				</ul>
			</li>
		</ul>

		<hr/>

		<p>
			<img src="${pageContext.request.contextPath}/resources/images/2013_2017.png" style="width: 100%;">
		</p>

</nav>
