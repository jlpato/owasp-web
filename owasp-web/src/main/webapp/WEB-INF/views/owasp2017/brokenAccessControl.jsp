<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>

    	<h1>A5 - 2017 Broken Access Control</h1>
    	
    	<hr/>
    	<h1>Insecure Direct Object References</h1>

		<h2><hr/>Referencias inseguras directas de objetos en servicio REST</h2>
		<p>En el gateway se realiza la autenticaci�n y autorizaci�n en cada petici�n debido a la naturaleza de un servicio REST,
			y cada recurso puede permitirse �nicamente al rol o usuario definido para consumirlo.</p>

		<div class="square">
			<h2>Spring Security</h2>
		  <p>
		  	<!-- HTML generated using hilite.me -->
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">spring</span><span style="color: #333333">.</span><span style="color: #0000CC">config</span><span style="color: #333333">;</span>
<span style="color: #333333">...</span>
<span style="color: #555555; font-weight: bold">@Configuration</span>
<span style="color: #555555; font-weight: bold">@EnableWebSecurity</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">SecurityConfig</span> <span style="color: #008800; font-weight: bold">extends</span> WebSecurityConfigurerAdapter <span style="color: #333333">{</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">protected</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">configure</span><span style="color: #333333">(</span>HttpSecurity http<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> Exception <span style="color: #333333">{</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;configure(HttpSecurity http)&quot;</span><span style="color: #333333">);</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
    	http<span style="color: #333333">.</span><span style="color: #0000CC">authorizeRequests</span><span style="color: #333333">()</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">antMatchers</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/person/**&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">hasRole</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ADMIN&quot;</span><span style="color: #333333">)</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">httpBasic</span><span style="color: #333333">().</span><span style="color: #0000CC">realmName</span><span style="color: #333333">(</span>Constants<span style="color: #333333">.</span><span style="color: #0000CC">SECURITY_REALM</span><span style="color: #333333">)</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">authenticationEntryPoint</span><span style="color: #333333">(</span>getBasicAuthEntryPoint<span style="color: #333333">())</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">sessionManagement</span><span style="color: #333333">().</span><span style="color: #0000CC">sessionCreationPolicy</span><span style="color: #333333">(</span>SessionCreationPolicy<span style="color: #333333">.</span><span style="color: #0000CC">STATELESS</span><span style="color: #333333">);</span>
    <span style="color: #333333">}</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>

			<!-- HTML generated using hilite.me -->
		</div>


		<h2><hr/>Referencias inseguras directas de objetos en la aplicaci�n web</h2>
		<p>En las aplicaciones web se realiza la autenticaci�n y posteriormente la autorizaci�n en cada petici�n,
		se debe implementar el uso de un Mapa de Referencias de Accesos para urls que lo requieran.</p>
		<p>Se deben evitar urls como la siguiente:</p>
		<img alt="insecure direct object reference" src="${pageContext.request.contextPath}/resources/images/bad_url.png"/>

		<div class="square">
			<h2>ESAPI</h2>
		  <p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17</pre></td><td><pre style="margin: 0; line-height: 125%">MyObject obj<span style="color: #333333">;</span>	 	<span style="color: #888888">// generate your object</span>
Collection coll<span style="color: #333333">;</span> 	<span style="color: #888888">// holds objects for display in UI</span>

<span style="color: #888888">//create ESAPI random access reference map</span>
AccessReferenceMap map <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> RandomAccessReferenceMap<span style="color: #333333">();</span>

<span style="color: #888888">//get indirect reference using direct reference as seed input</span>
String indirectReference <span style="color: #333333">=</span> map<span style="color: #333333">.</span><span style="color: #0000CC">addDirectReference</span><span style="color: #333333">(</span>obj<span style="color: #333333">.</span><span style="color: #0000CC">getId</span><span style="color: #333333">());</span>

<span style="color: #888888">//set indirect reference for each object - requires your app object to have this method</span>
obj<span style="color: #333333">.</span><span style="color: #0000CC">setIndirectReference</span><span style="color: #333333">(</span>indirectReference<span style="color: #333333">);</span>

<span style="color: #888888">//add object to display collection</span>
coll<span style="color: #333333">.</span><span style="color: #0000CC">add</span><span style="color: #333333">(</span>obj<span style="color: #333333">);</span>

<span style="color: #888888">//store collection in request/session and forward to UI</span>
<span style="color: #333333">...</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
		</div>


<hr/>
<h1>Missing Function Level Access Control</h1>

		<h2><hr/>Control de acceso inexistente a nivel de funci�n.</h2>
		<p>La mayor�a de las aplicaciones web verifican los permisos de acceso a nivel de funci�n antes de hacer
			que esa funcionalidad sea visible en la interfaz de usuario.
			Sin embargo, las aplicaciones deben realizar las mismas comprobaciones de control de acceso en el servidor
			cuando se accede a cada funci�n.
			Si las solicitudes no se verifican, los atacantes podr�n falsificar las solicitudes para acceder a
			la funcionalidad sin la debida autorizaci�n.</p>

		<h2><hr/>Spring Security</h2>
		<p>Proporciona un mecanismo de autorizaci�n en la interfaz gr�fica como dentro de la aplicaci�n del lado del servidor.</p>

		<div class="square">
			<h2>Spring Security en servicio REST</h2>
			<p>En un servicio REST no hay interfaz gr�fica, solo se realiza la validaci�n en el servidor
				sobre los permisos que tiene el usuario para utilizar las operaciones definidas.</p>
		  <p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
PersonController.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">controller</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #555555; font-weight: bold">@RestController</span>
<span style="color: #555555; font-weight: bold">@RequestMapping</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/person&quot;</span><span style="color: #333333">)</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">PersonController</span> <span style="color: #333333">{</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #555555; font-weight: bold">@RequestMapping</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/find/{id}&quot;</span><span style="color: #333333">)</span>
	<span style="color: #555555; font-weight: bold">@PreAuthorize</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;hasRole(&#39;ADMIN&#39;)&quot;</span><span style="color: #333333">)</span>
	<span style="color: #008800; font-weight: bold">public</span> ResponseEntity<span style="color: #333333">&lt;</span>Person<span style="color: #333333">&gt;</span> <span style="color: #0066BB; font-weight: bold">getPersonDetail</span><span style="color: #333333">(</span> <span style="color: #555555; font-weight: bold">@PathVariable</span> Integer id <span style="color: #333333">)</span> <span style="color: #333333">{</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #333333">}</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>

			<!-- HTML generated using hilite.me -->
		</div>

		<div class="square">
			<h2>Spring Security en aplicaciones web</h2>
			<p>En las aplicaciones web tambi�n se implementa la autorizaci�n a nivel de m�todos.</p>
		  <p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
SecurityConfig.java
<table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3
4
5
6
7
8</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">pacrdd</span><span style="color: #333333">.</span><span style="color: #0000CC">spring</span><span style="color: #333333">.</span><span style="color: #0000CC">config</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #555555; font-weight: bold">@Configuration</span>
<span style="color: #555555; font-weight: bold">@EnableWebSecurity</span>
<span style="color: #555555; font-weight: bold">@EnableGlobalMethodSecurity</span><span style="color: #333333">(</span>prePostEnabled<span style="color: #333333">=</span><span style="color: #008800; font-weight: bold">true</span><span style="color: #333333">)</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">SecurityConfig</span> <span style="color: #008800; font-weight: bold">extends</span> WebSecurityConfigurerAdapter <span style="color: #333333">{</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>   
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
			
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
HelloController.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">pacrdd</span><span style="color: #333333">.</span><span style="color: #0000CC">spring</span><span style="color: #333333">.</span><span style="color: #0000CC">controller</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #555555; font-weight: bold">@Controller</span>
<span style="color: #555555; font-weight: bold">@RequestMapping</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/&quot;</span><span style="color: #333333">)</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">HelloController</span> <span style="color: #333333">{</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #555555; font-weight: bold">@GetMapping</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/admin&quot;</span><span style="color: #333333">)</span>
	<span style="color: #555555; font-weight: bold">@PreAuthorize</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;hasRole(&#39;ADMIN&#39;)&quot;</span><span style="color: #333333">)</span>
	<span style="color: #008800; font-weight: bold">public</span> String <span style="color: #0066BB; font-weight: bold">admin</span><span style="color: #333333">(</span>ModelMap model<span style="color: #333333">)</span> <span style="color: #333333">{</span>
		LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span> <span style="background-color: #fff0f0">&quot;HelloController::admin&quot;</span> <span style="color: #333333">);</span>
		<span style="color: #008800; font-weight: bold">return</span> <span style="background-color: #fff0f0">&quot;admin.admin&quot;</span><span style="color: #333333">;</span>
	<span style="color: #333333">}</span>
	
	<span style="color: #555555; font-weight: bold">@GetMapping</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/products&quot;</span><span style="color: #333333">)</span>
	<span style="color: #555555; font-weight: bold">@PreAuthorize</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;isAuthenticated()&quot;</span><span style="color: #333333">)</span>
    	<span style="color: #008800; font-weight: bold">public</span> String <span style="color: #0066BB; font-weight: bold">products</span><span style="color: #333333">(</span>ModelMap model<span style="color: #333333">)</span> <span style="color: #333333">{</span>
		LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span> <span style="background-color: #fff0f0">&quot;HelloController::products&quot;</span> <span style="color: #333333">);</span>		
		<span style="color: #008800; font-weight: bold">return</span> <span style="background-color: #fff0f0">&quot;products&quot;</span><span style="color: #333333">;</span>
	<span style="color: #333333">}</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
		  	<!-- HTML generated using hilite.me -->

			<p>Y  adicional, tambi�n con Spring Security, se implementa la autorizaci�n a nivel GUI mediante  Security Tags.</p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
menu.jsp
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">&lt;%@</span> taglib prefix<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;tiles&quot;</span> uri<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;http://tiles.apache.org/tags-tiles&quot;</span><span style="color: #008800; font-weight: bold">%&gt;</span>
<span style="color: #008800; font-weight: bold">&lt;%@</span> taglib prefix<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;sec&quot;</span> uri<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;http://www.springframework.org/security/tags&quot;</span> <span style="color: #008800; font-weight: bold">%&gt;</span>
<span style="color: #007700">&lt;nav&gt;</span>
    ...
    <span style="color: #007700">&lt;ul</span> <span style="color: #0000CC">id=</span><span style="background-color: #fff0f0">&quot;menu&quot;</span><span style="color: #007700">&gt;</span>
        <span style="color: #007700">&lt;li&gt;&lt;a</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;${pageContext.request.contextPath}/&quot;</span><span style="color: #007700">&gt;</span>Home<span style="color: #007700">&lt;/a&gt;&lt;/li&gt;</span>
        <span style="color: #007700">&lt;sec:authorize</span> <span style="color: #0000CC">access=</span><span style="background-color: #fff0f0">&quot;hasRole(&#39;ADMIN&#39;)&quot;</span><span style="color: #007700">&gt;</span>
            <span style="color: #007700">&lt;li&gt;&lt;a</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;${pageContext.request.contextPath}/admin&quot;</span><span style="color: #007700">&gt;</span>Admin<span style="color: #007700">&lt;/a&gt;&lt;/li&gt;</span>
        <span style="color: #007700">&lt;/sec:authorize&gt;</span>
       <span style="color: #007700">&lt;li&gt;&lt;a</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;${pageContext.request.contextPath}/products&quot;</span><span style="color: #007700">&gt;</span>Products<span style="color: #007700">&lt;/a&gt;&lt;/li&gt;</span>
       <span style="color: #007700">&lt;li&gt;&lt;a</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;${pageContext.request.contextPath}/contactus&quot;</span><span style="color: #007700">&gt;</span>Contact Us<span style="color: #007700">&lt;/a&gt;&lt;/li&gt;</span>
...
    <span style="color: #007700">&lt;/ul&gt;</span>
<span style="color: #007700">&lt;/nav&gt;</span>
</pre></td></tr></table></div>
		  	<!-- HTML generated using hilite.me -->
		</div>


</nav>
