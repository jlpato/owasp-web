<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A7 - 2017 Cross Site Scripting (XSS)</h1>
    	<p>XSS es la falla mas predominante en la seguridad de aplicaciones web.</p>
    	<p>Las fallas de XSS ocurren cuando una aplicaci�n incluye datos proporcionados
    		por el usuario en una p�gina a enviar al navegador, sin propiamente validar o formatear el contenido.</p>
    	<p>Hay tres tipos conocidos de fallas XSS:</p>
    		<lu>
    			<li><i>Stored</i></li>
    			<li><i>Reflected</i></li>
    			<li><i>DOM based XSS</i></li>
    		</lu>
		<h2><hr/>Validando las entradas de usuario</h2>
		<p>Convertir todos los caracteres especiales en sus correspondientes referencias de entidades HTML.</p>

		<div class="square">
			<h2>Validar cuando el elemento del formulario es ligado al formulario.</h2>
		  <p>

<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">java.beans.PropertyEditorSupport</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.web.util.HtmlUtils</span><span style="color: #333333">;</span>

<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">HtmlEscapeStringEditor</span> <span style="color: #008800; font-weight: bold">extends</span> PropertyEditorSupport <span style="color: #333333">{</span>
    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">setAsText</span><span style="color: #333333">(</span>String text<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> IllegalArgumentException <span style="color: #333333">{</span>
        String out <span style="color: #333333">=</span> <span style="background-color: #fff0f0">&quot;&quot;</span><span style="color: #333333">;</span>
        <span style="color: #008800; font-weight: bold">if</span><span style="color: #333333">(</span>text <span style="color: #333333">!=</span> <span style="color: #008800; font-weight: bold">null</span><span style="color: #333333">)</span>
            out <span style="color: #333333">=</span> HtmlUtils<span style="color: #333333">.</span><span style="color: #0000CC">htmlEscape</span><span style="color: #333333">(</span>text<span style="color: #333333">.</span><span style="color: #0000CC">trim</span><span style="color: #333333">());</span>

        setValue<span style="color: #333333">(</span>out<span style="color: #333333">);</span>
    <span style="color: #333333">}</span>

    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">public</span> String <span style="color: #0066BB; font-weight: bold">getAsText</span><span style="color: #333333">()</span> <span style="color: #333333">{</span>
        String out <span style="color: #333333">=</span> <span style="color: #333333">(</span>String<span style="color: #333333">)</span> getValue<span style="color: #333333">();</span>
        <span style="color: #008800; font-weight: bold">if</span><span style="color: #333333">(</span>out <span style="color: #333333">==</span> <span style="color: #008800; font-weight: bold">null</span><span style="color: #333333">)</span>
            out <span style="color: #333333">=</span> <span style="background-color: #fff0f0">&quot;&quot;</span><span style="color: #333333">;</span>
        <span style="color: #008800; font-weight: bold">return</span> out<span style="color: #333333">;</span>
    <span style="color: #333333">}</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->

</div>


		<div class="square">
			<h2>Validando todas las peticiones con filtros</h2>
			<p>Usando un filtro XSS para eliminar todos los par�metros maliciosos en el request usando escape HTML</p>
		  <p>

		  <!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">XSSFilter</span> <span style="color: #008800; font-weight: bold">implements</span> Filter <span style="color: #333333">{</span>

    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">init</span><span style="color: #333333">(</span>FilterConfig filterConfig<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> ServletException <span style="color: #333333">{</span>
    <span style="color: #333333">}</span>

    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">destroy</span><span style="color: #333333">()</span> <span style="color: #333333">{</span>
    <span style="color: #333333">}</span>

    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">doFilter</span><span style="color: #333333">(</span>ServletRequest request<span style="color: #333333">,</span> ServletResponse response<span style="color: #333333">,</span> FilterChain chain<span style="color: #333333">)</span>
        <span style="color: #008800; font-weight: bold">throws</span> IOException<span style="color: #333333">,</span> ServletException <span style="color: #333333">{</span>
        chain<span style="color: #333333">.</span><span style="color: #0000CC">doFilter</span><span style="color: #333333">(</span><span style="color: #008800; font-weight: bold">new</span> XSSRequestWrapper<span style="color: #333333">((</span>HttpServletRequest<span style="color: #333333">)</span> request<span style="color: #333333">),</span> response<span style="color: #333333">);</span>
    <span style="color: #333333">}</span>

<span style="color: #333333">}</span>
</pre></td></tr></table></div>

<!-- HTML generated using hilite.me -->
</div>



		<hr/><h2><hr/>Validando las salidas</h2>
		<p>Spring soporta esta funcionalidad directamente en tres niveles diferentes:</p>


		<div class="square">
			<h2>A nivel aplicaci�n</h2>
			<p>Especificando un par�metro de contexto llamado <i>defaultHtmlEscape</i></p>
		  <p>

		  <!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3
4</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #333333">&lt;</span>context<span style="color: #333333">-</span>param<span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;</span>param<span style="color: #333333">-</span>name<span style="color: #333333">&gt;</span>defaultHtmlEscape<span style="color: #333333">&lt;/</span>param<span style="color: #333333">-</span>name<span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;</span>param<span style="color: #333333">-</span>value<span style="color: #333333">&gt;</span><span style="color: #008800; font-weight: bold">true</span><span style="color: #333333">&lt;/</span>param<span style="color: #333333">-</span>value<span style="color: #333333">&gt;</span>
<span style="color: #333333">&lt;/</span>context<span style="color: #333333">-</span>param<span style="color: #333333">&gt;</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->
</div>


		<div class="square">
			<h2>A nivel p�gina</h2>
			<p>Especificando una declaraci�n de escape de HTML</p>
		  <p>

		  <!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #333333">&lt;</span><span style="color: #997700; font-weight: bold">spring:</span>htmlEscape defaultHtmlEscape<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;true&quot;</span> <span style="color: #333333">/&gt;</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->
</div>

		<div class="square">
			<h2>A nivel formulario</h2>
			<p>Especificando el atributo <i>htmlEscape</i> del formulario</p>
		  <p>

		  <!-- HTML generated using hilite.me -->
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #333333">&lt;</span><span style="color: #997700; font-weight: bold">form:</span>input path<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;name&quot;</span> htmlEscape<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;true&quot;</span> <span style="color: #333333">/&gt;</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->
</div>


</nav>
