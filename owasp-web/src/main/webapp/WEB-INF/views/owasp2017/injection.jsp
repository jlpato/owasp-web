<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A1 - 2017 Injection</h1>

		<hr/>
		<h2>Inyecci&oacute;n de SQL</h2>
		<p>Las fallas de inyecci&oacute;n, como SQL, NoSQL, OS o LDAP ocurren cuando se env&iacute;an datos no
		confiables a un int&eacute;rprete, como parte de un comando o consulta. Los datos da&ntilde;inos del atacante
		pueden enga&ntilde;ar al int&eacute;rprete para que ejecute comandos involuntarios o acceda a los datos sin
		la debida autorizaci&oacute;n.
		</p>
		<p>Inyecci&oacute;n de SQL es una t&eacute;cnica de Inyecci&oacute;n de c&oacute;digo que puede destruir una base de datos.</p>
		<p>Inyecci&oacute;n de SQL es una de las t&eacute;cnicas m&aacute;s comunes de hacking.</p>
		<p>Inyecci&oacute;n de SQL es la colocaci&oacute;n de c&oacute;digo malicioso en sentencias SQL, a trav&eacute;s de entradas de p&aacute;ginas web.</p>

		<div class="square">
			<h2>Spring JBDC Template</h2>
			<p>Spring JBDC Template emplea <i>par&aacute;metros posicionados</i> ('?') o <i>par&aacute;metros nombrados</i> (':userName')
				como argumentos para evitar ataques de inyecci&oacute;n de c&oacute;digo
				indicando a JDBC usar <i>bind variables</i>
			</p>
			<p>SQL injection is the placement of malicious code in SQL statements, via web page input.</p>
		  <p>

		  <!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
PersonRepository.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">owasp</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">repository</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #555555; font-weight: bold">@Repository</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">PersonRepository</span> <span style="color: #333333">{</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #008800; font-weight: bold">public</span> com<span style="color: #333333">.</span><span style="color: #0000CC">owasp</span><span style="color: #333333">.</span><span style="color: #0000CC">model</span><span style="color: #333333">.</span><span style="color: #0000CC">Person</span> <span style="color: #0066BB; font-weight: bold">create</span><span style="color: #333333">(</span><span style="color: #008800; font-weight: bold">final</span> Person person<span style="color: #333333">)</span> <span style="color: #333333">{</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
		<span style="color: #008800; font-weight: bold">this</span><span style="color: #333333">.</span><span style="color: #0000CC">jdbcTemplate</span><span style="color: #333333">.</span><span style="color: #0000CC">update</span><span style="color: #333333">(</span>
				<span style="background-color: #fff0f0">&quot;insert into person ( id, location, name) values ( ?, ?, ? )&quot;</span><span style="color: #333333">,</span>
				primaryKey<span style="color: #333333">,</span>
				person<span style="color: #333333">.</span><span style="color: #0000CC">getLocation</span><span style="color: #333333">(),</span>
				person<span style="color: #333333">.</span><span style="color: #0000CC">getName</span><span style="color: #333333">());</span>
		person<span style="color: #333333">.</span><span style="color: #0000CC">setId</span><span style="color: #333333">(</span> primaryKey <span style="color: #333333">);</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #333333">}</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->

</div>


		<div class="square">
			<h2>Hibernate</h2>
			<p>Hibernate nos ayuda a evitar la inyecci&oacute;n de c&oacute;digo proporcionando mec&aacute;nismos como <i>Named Parameters</i> o <i>Criterias</i></p>
			<p>En las aplicaciones web, se puede implementar la persistencia con Hibernate y JPA, que emplea POJO's para realizar las operaciones hacia la base de datos.</p>
			<p>SQL injection is the placement of malicious code in SQL statements, via web page input.</p>
		  <p>

		  <!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
PersonRepository.java
<table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3
4
5</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">owasp</span><span style="color: #333333">.</span><span style="color: #0000CC">persistence</span><span style="color: #333333">;</span>
<span style="color: #333333">...</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">interface</span> <span style="color: #BB0066; font-weight: bold">PersonRepository</span> <span style="color: #008800; font-weight: bold">extends</span> JpaRepository<span style="color: #333333">&lt;</span>Person<span style="color: #333333">,</span> Long<span style="color: #333333">&gt;{</span>

<span style="color: #333333">}</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->
</div>

		<h2><hr/>Inyecci&oacute;n de LDAP</h2>
		<p>Inyecci&oacute;n de LDAP es un ataque utilizado para explotar aplicaciones basadas en web que construyen sentencias LDAP basados en la entrada de los usuarios.</p>
		<p>Cuando una aplicaci�n falla en desinfectar la entrada del usuario, es posible modificar la sentencia LDAP usando un proxy local. Esto puede resultar en la ejecuci�n de comandos arbitrarios tales como otorgar permisos a consultas no autorizadas, y modificaci�n del contenido dentro del �rbol LDAP.</p>
		<p>Las mismas t�cnicas de explotaci�n avanzadas disponibles en inyecci�n de SQL pueden aplicarse similarmente en la inyecci�n de LDAP.</p>

		<div class="square">
			<h2>Spring LDAP Template</h2>
			<p>Spring LDAP esta dise�ado para simplificar la programaci�n de LDAP en Java. Algunas de las funcionalidades proporcionadas por la librer�a son:</p>
			<ul>
				<li>Estilo JDBC Template, plantilla para simplificar la programaci�n de LDAP.</li>
				<li>JPA/Hibernate, mapeo de objetos/directorios basado en anotaciones.</li>
				<li>Soporte para repositorios de Spring Data, inlcluido el soporte para QueryDSL.</li>
				<li>Utiler�as para simplificar la construcci�n de consultas LDAP y <i>distinguished names</i>.</li>
				<li>Pool de conexiones LDAP apropiado.</li>
				<li>Soporte de transacciones en el cliente LDAP.</li>
			</ul>
		  <p>

		  <!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">example</span><span style="color: #333333">.</span><span style="color: #0000CC">repo</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">static</span> org<span style="color: #333333">.</span><span style="color: #0000CC">springframework</span><span style="color: #333333">.</span><span style="color: #0000CC">ldap</span><span style="color: #333333">.</span><span style="color: #0000CC">query</span><span style="color: #333333">.</span><span style="color: #0000CC">LdapQueryBuilder</span><span style="color: #333333">.</span><span style="color: #0000CC">query</span><span style="color: #333333">;</span>

<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">PersonRepoImpl</span> <span style="color: #008800; font-weight: bold">implements</span> PersonRepo <span style="color: #333333">{</span>
   <span style="color: #008800; font-weight: bold">private</span> LdapTemplate ldapTemplate<span style="color: #333333">;</span>

   <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">setLdapTemplate</span><span style="color: #333333">(</span>LdapTemplate ldapTemplate<span style="color: #333333">)</span> <span style="color: #333333">{</span>
      <span style="color: #008800; font-weight: bold">this</span><span style="color: #333333">.</span><span style="color: #0000CC">ldapTemplate</span> <span style="color: #333333">=</span> ldapTemplate<span style="color: #333333">;</span>
   <span style="color: #333333">}</span>

   <span style="color: #008800; font-weight: bold">public</span> List<span style="color: #333333">&lt;</span>String<span style="color: #333333">&gt;</span> <span style="color: #0066BB; font-weight: bold">getAllPersonNames</span><span style="color: #333333">()</span> <span style="color: #333333">{</span>
      <span style="color: #008800; font-weight: bold">return</span> ldapTemplate<span style="color: #333333">.</span><span style="color: #0000CC">search</span><span style="color: #333333">(</span>
         query<span style="color: #333333">().</span><span style="color: #0000CC">where</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;objectclass&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">is</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;person&quot;</span><span style="color: #333333">),</span>
         <span style="color: #008800; font-weight: bold">new</span> AttributesMapper<span style="color: #333333">&lt;</span>String<span style="color: #333333">&gt;()</span> <span style="color: #333333">{</span>
            <span style="color: #008800; font-weight: bold">public</span> String <span style="color: #0066BB; font-weight: bold">mapFromAttributes</span><span style="color: #333333">(</span>Attributes attrs<span style="color: #333333">)</span>
               <span style="color: #008800; font-weight: bold">throws</span> NamingException <span style="color: #333333">{</span>
               <span style="color: #008800; font-weight: bold">return</span> attrs<span style="color: #333333">.</span><span style="color: #0000CC">get</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;cn&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">get</span><span style="color: #333333">().</span><span style="color: #0000CC">toString</span><span style="color: #333333">();</span>
            <span style="color: #333333">}</span>
         <span style="color: #333333">});</span>
   <span style="color: #333333">}</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
<!-- HTML generated using hilite.me -->
</div>


		<hr/>
		<h2>Inyecci&oacute;n de XPath</h2>
		<p>Es similar a inyecci�n de SQL, los ataques de inyecci�n de XPath ocurren cuando un sitio web usa informaci�n proporcionada por el usuario para construir una consulta XPath para datos XML.</p>

		<p>Vamos a ssumir que  un mensaje SOAP entrega un ID de cliente a la l&oacute;gica de la aplicac&iacute;n, consultando un XML con toda la informaci&oacute;n de los clientes.
		La aplicaci&oacute;n toma la siguiente entrada:</p>
		<p><code>//users/custid[123]</code></p>
		<p>Xpath query:</p>
		<p><code>//users/custid[./age>0]</code></p>
		<p>As&iacute; como con los otros ataques de inyecci&oacute;n, se deben validar las entradas y evitar los caracteres especiales en medida de lo posible.</p>
</nav>
