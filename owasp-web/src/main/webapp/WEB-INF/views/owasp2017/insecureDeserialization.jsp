<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A8 - 2017 Insecure Deserialization</h1>

		<hr/>
		<h2>Deserializaci&oacute;n insegura</h2>
		<p>Estos defectos ocurren cuando una aplicaci&oacute;n recibe <mark>objetos serializados da&ntilde;inos</mark> y estos objetos
		pueden ser manipulados o borrados por el atacante para realizar ataques de repetici&oacute;n, inyecciones o elevar
		sus privilegios de ejecuci&oacute;n. En el peor de los casos, la deserializaci&oacute;n insegura puede conducir a
		la ejecuci&oacute;n remota de c&oacute;digo en el servidor.</p>
		
		<h3>Escenario de ataque 1</h3>
		<p>Una aplicación React invoca a un conjunto de microservicios Spring Boot. Siendo programadores funcionales,
		 intentaron asegurar que su c&oacute;digo sea inmutable. La soluci&oacute;n a la que llegaron es serializar
		 el estado del usuario y pasarlo en ambos sentidos con cada solicitud. Un atacante advierte la firma <code>R00</code> del objeto Java,
		 y usa la herramienta Java Serial Killer para obtener ejecuci&oacute;n de c&oacute;digo remoto en el servidor de la aplicaci&oacute;n.</p>
		
		<h3>Escenario de ataque 2</h3>
		<p>Un foro utiliza serializaci&oacute;n de objetos para almacenar una "super cookie", conteniendo el ID, rol, hash
		de la contrase&ntilde;a y otros estados del usuario:
		<code>a:4:{i:0;i:132;i:1;s:7:<mark>"Mallory"</mark>;i:2;s:4:<mark>"user"</mark>;i:3;s:32:"b6a8b3bea87fe0e05022f8f3c88bc960";}</code>
		Un atacante modifica el objeto serializado para darse privilegios de administrador a s&iacute; mismo:
		<code>a:4:{i:0;i:1;i:1;s:5:<mark>"Alice"</mark>;i:2;s:5:<mark>"admin"</mark>;i:3;s:32:"b6a8b3bea87fe0e05022f8f3c88bc960";}</code></p>
		
		<hr/>
		<h2>Java</h2>
		<p>Las siguientes t&eacute;cnicas son buenas para prevenir ataques de deserializaci&oacute;n en java:</p>
		
		<h3>Consejos de implementaci&oacute;n</h3>
		<ul>
			<li>En el c&oacute;digo, se debe sobreescribir el m&eacute;todo <code>ObjectInputStream#resolveClass()</code> para prevenir a clases arbitrarias de ser deserializadas.
			Este comportamiento puede ser envuelto e una librer&iacute;a como <a target="_blank" href="https://github.com/ikkisoft/SerialKiller">SerialKiller</a></li>
			<li>Se debe usar un reemplazo para el m&eacute;todo gen&eacute;rico <code>readObject()</code>. Se deben evitar ataques como
			<a target="_blank" href="https://en.wikipedia.org/wiki/Billion_laughs_attack"> Billion laughs attack</a> checando la longitud y el n&uacute;mero de objetos deserializados.</li>
		</ul>
		
		<h3>WhiteBox</h3>
		<p>Se debe tener cuidado del uso de las siguientes API's potencialmente vulnerables a ataques de deserializaci&oacute;n.</p>
		<ol>
			<li><code>XMLdecoder</code> con par&aacute;metros externos.</li>
			<li><code>XStream</code> con el m&eacute;todo <code>fromXML</code>,  (xstream version <= v1.46 es vulnerable al incidente de serializaci&oacute;n).</li>
			<li><code>ObjectInputSteam#readObject</code></li>
			<li>Usos de <code>readObject</code>, <code>readObjectNodData</code>, <code>readResolve</code> o <code>readExternal</code>.</li>
			<li><code>ObjectInputStream.readUnshared</code>.</li>
			<li><code>Serializable</code></li>
		</ol>
		
		<h3>BlackBox</h3>
		<p>Si los datos de tr&aacute;fico capturados incluyen los siguientes patrones es probable que la informaci&oacute;n
		haya sido enviada en <code>Streams</code> de serializaci&oacute;n de java:</p>
		<ul>
			<li><code>AC ED 00 05</code> en hexad&eacute;decimal.</li>
			<li><code>rO0</code> en Base64.</li>
			<li><code>Content-type</code> header de una respuesta HTTP con el valor <code>application/x-java-serialized-object</code>.</li>
		</ul>

		<h3>Prevenir la fuga de datos y la invasi&oacute;n de campos de confianza</h3>
		<p>Usar <code>transient</code></p>
		<p>Para una clase que es definida como <code>Serializable</code>, los atributos de informaci&oacute;n sensible deben ser
		declarados como <code>private transient</code>.</p>
<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3
4
5
6</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">myAccount</span> <span style="color: #008800; font-weight: bold">implements</span> Serializable
<span style="color: #333333">{</span>
    <span style="color: #008800; font-weight: bold">private</span> <span style="color: #008800; font-weight: bold">transient</span> <span style="color: #333399; font-weight: bold">double</span> profit<span style="color: #333333">;</span> <span style="color: #888888">// declared transient</span>

    <span style="color: #008800; font-weight: bold">private</span> <span style="color: #008800; font-weight: bold">transient</span> <span style="color: #333399; font-weight: bold">double</span> margin<span style="color: #333333">;</span> <span style="color: #888888">// declared transient</span>
    <span style="color: #333333">....</span>
</pre></td></tr></table>
</div>

		<h3>Prevenir la deserializaci&oacute;n de objetos de dominio</h3>
		<p>Algunos de los objetos de la aplicaci&oacute; son forzados a implementar <code>Serializable</code> debido a su jerarqu&iacute;a de clases.
		Para garantizar que los objetos no puedan ser deserializables, se debe declarar el m&eacute;todo <code>readObject()</code>
		(con modificador <code>final</code>) el cual lanzar&aacute; una excepci&oacute;n:</p>
		
<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">private</span> <span style="color: #008800; font-weight: bold">final</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">readObject</span><span style="color: #333333">(</span>ObjectInputStream in<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> java<span style="color: #333333">.</span><span style="color: #0000CC">io</span><span style="color: #333333">.</span><span style="color: #0000CC">IOException</span> <span style="color: #333333">{</span>
    <span style="color: #008800; font-weight: bold">throw</span> <span style="color: #008800; font-weight: bold">new</span> java<span style="color: #333333">.</span><span style="color: #0000CC">io</span><span style="color: #333333">.</span><span style="color: #0000CC">IOException</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;Cannot be deserialized&quot;</span><span style="color: #333333">);</span>
<span style="color: #333333">}</span>
</pre></td></tr></table>
</div>
		
		<h3>Personalizar la clase <code>java.io.ObjectInputStream</code></h3>
		<p>La clase <code>java.io.ObjectInputStream</code> es utlizada para deserializar objetos. Es posible personalizar y asegurar
		su comportamiento hered&aacute;ndolo. Esta es la mejor soluci&oacute;n si:</p>
		<ul>
			<li>Podemos cambiar el c&oacute;digo que realiza la deserializaci&oacute;n.</li>
			<li>Se conocen las clases que se espera deserializar.</li>
		</ul>
		<p>La idea general es sobreescribir el m&eacute;todo <code>ObjectInputStream#resolveClass()</code>
		para restringir las clases que est&aacute;n permitidas para su deserializaci&oacute;n.</p>
		<p>Este m&eacute;todo se ejecuta previo al llamado a <code>readObject()</code>, de este modo aseguramos que la actividad
		de deserializaci&oacute;n solo ocurre cuando se trata de un tipo de clase que expl&iacute;citamente permitimos.</p>
		
<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">LookAheadObjectInputStream</span> <span style="color: #008800; font-weight: bold">extends</span> ObjectInputStream <span style="color: #333333">{</span>

    <span style="color: #008800; font-weight: bold">public</span> <span style="color: #0066BB; font-weight: bold">LookAheadObjectInputStream</span><span style="color: #333333">(</span>InputStream inputStream<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> IOException <span style="color: #333333">{</span>
        <span style="color: #008800; font-weight: bold">super</span><span style="color: #333333">(</span>inputStream<span style="color: #333333">);</span>
    <span style="color: #333333">}</span>

    <span style="color: #888888">/**</span>
<span style="color: #888888">    * Only deserialize instances of our expected Bicycle class</span>
<span style="color: #888888">    */</span>
    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">protected</span> Class<span style="color: #333333">&lt;?&gt;</span> resolveClass<span style="color: #333333">(</span>ObjectStreamClass desc<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> IOException<span style="color: #333333">,</span> ClassNotFoundException <span style="color: #333333">{</span>
        <span style="color: #008800; font-weight: bold">if</span> <span style="color: #333333">(!</span>desc<span style="color: #333333">.</span><span style="color: #0000CC">getName</span><span style="color: #333333">().</span><span style="color: #0000CC">equals</span><span style="color: #333333">(</span>Bicycle<span style="color: #333333">.</span><span style="color: #0000CC">class</span><span style="color: #333333">.</span><span style="color: #0000CC">getName</span><span style="color: #333333">()))</span> <span style="color: #333333">{</span>
            <span style="color: #008800; font-weight: bold">throw</span> <span style="color: #008800; font-weight: bold">new</span> <span style="color: #0066BB; font-weight: bold">InvalidClassException</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;Unauthorized deserialization attempt&quot;</span><span style="color: #333333">,</span> desc<span style="color: #333333">.</span><span style="color: #0000CC">getName</span><span style="color: #333333">());</span>
        <span style="color: #333333">}</span>
        <span style="color: #008800; font-weight: bold">return</span> <span style="color: #008800; font-weight: bold">super</span><span style="color: #333333">.</span><span style="color: #0000CC">resolveClass</span><span style="color: #333333">(</span>desc<span style="color: #333333">);</span>
    <span style="color: #333333">}</span>
<span style="color: #333333">}</span>
</pre></td></tr></table>
</div>


		<h3>Personalizar globalmente todas las clases <code>java.io.ObjectInputStream</code> con un Agente Java</h3>
		<p>Como se mencion&oacute; en el punto anterior,, la clase <code>java.io.ObjectInputStream</code> es utilizada para deserializar objetos.
		Es posible endurecer su comportamiento hered&aacute;ndola. De cualquier modo, si no tenemos el c&oacute;digo
		(quiza de alg&uacute;n servidor de aplicaciones) o no podemos esperar para la actualizaci&oacute;n de seguridad que resuelva el incidente,
		usar un Agente para endurecer los objetos <code>java.io.ObjectInputStream</code> es la mejor soluci&oacute;n.</p>
		<p>Cambiando globalmente a <code>java.io.ObjectInputStream</code> es seguro para listas negras de clases de deserializaci&oacute;n,
		debido a que no conocemos todas las clases que las aplicaciones tienen proyectado deserializar.</p>
		<p>Afortunadamente son pocas las clases que necesitan estar en la lista negra, por ahora.</p>
		<p>Miembros de la comunidad han desarrollado el siguiente Agente:
		<a target="_blank" href="https://github.com/Contrast-Security-OSS/contrast-rO0">rO0 by Contrast Security</a></p>
		<p>Otro agente similar pero con un enfoque menos escalable es el siguiente
		<a target="_blank" href="https://github.com/wsargent/paranoid-java-serialization">Paranoid Java Serialization</a></p>
		
		
		<br/><br/>
		<h2>Referencias</h2>
		<br/><a target="_blank" href="https://www.ibm.com/developerworks/library/se-lookahead/">https://www.ibm.com/developerworks/library/se-lookahead/</a>
		<br/><a target="_blank" href="https://www.synopsys.com/blogs/software-security/mitigate-java-deserialization-vulnerability-jboss/">mitigate-java-deserialization-vulnerability-jboss</a>
		<br/><a target="_blank" href="https://www.baeldung.com/java-instrumentation">Agentes Java</a>
		
		
		
		
</nav>
