<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<style>
<!--
-->
</style>
<nav>
    	
    	<h1>A10 - Insufficient Logging and Monitoring</h1>

		<hr/>
		<h2>Registro y monitoreo insuficientes</h2>
		<p>El registro y monitoreo insuficiente, junto a la falta de respuesta ante incidentes permiten a los
		atacantes mantener el ataque en el tiempo, pivotear a otros sistemas y manipular, extraer o
		destruir datos. Los estudios muestran que el tiempo de detecci&oacute;n de una brecha de seguridad es
		mayor a 200 d&iacute;as, siendo t&iacute;picamente detectado por terceros en lugar de por procesos internos.
		</p>
		
		<p>En 2016, <mark>la identificaci&oacute;n de brechas tard&oacute; una media de 191 d&iacute;as</mark>,
		un tiempo m&aacute;s que suficiente para infligir da&ntilde;o.</p>
		
<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">javax.servlet.http.HttpSessionEvent</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">javax.servlet.http.HttpSessionListener</span><span style="color: #333333">;</span>

<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.slf4j.Logger</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.slf4j.LoggerFactory</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.context.ApplicationEvent</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.context.ApplicationListener</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.security.authentication.event.AuthenticationSuccessEvent</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.security.core.session.SessionDestroyedEvent</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.security.core.userdetails.UserDetails</span><span style="color: #333333">;</span>
<span style="color: #008800; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">org.springframework.stereotype.Component</span><span style="color: #333333">;</span>

<span style="color: #555555; font-weight: bold">@Component</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">ApplicationListenerComponent</span>
<span style="color: #008800; font-weight: bold">implements</span> ApplicationListener<span style="color: #333333">&lt;</span>ApplicationEvent<span style="color: #333333">&gt;,</span> HttpSessionListener
<span style="color: #333333">{</span>
	
	<span style="color: #008800; font-weight: bold">private</span> <span style="color: #008800; font-weight: bold">static</span> <span style="color: #008800; font-weight: bold">final</span> Logger LOGGER <span style="color: #333333">=</span> LoggerFactory<span style="color: #333333">.</span><span style="color: #0000CC">getLogger</span><span style="color: #333333">(</span>ApplicationListenerComponent<span style="color: #333333">.</span><span style="color: #0000CC">class</span><span style="color: #333333">);</span>
	
	String msgPrefix <span style="color: #333333">=</span> <span style="background-color: #fff0f0">&quot;\n\n.........(0 0)\n.---oOO-- (_)-----.\n╔═════════════════╗\n&quot;</span><span style="color: #333333">;</span>
	String msgSuffix <span style="color: #333333">=</span> <span style="background-color: #fff0f0">&quot;\n╚═════════════════╝\n&#39;--------------oOO\n........|__|__|\n........ || ||\n....... ooO Ooo\n\n\n&quot;</span><span style="color: #333333">;</span>
	<span style="color: #008800; font-weight: bold">private</span> String <span style="color: #0066BB; font-weight: bold">msg</span><span style="color: #333333">(</span> String msg <span style="color: #333333">){</span>
		<span style="color: #008800; font-weight: bold">return</span> msgPrefix <span style="color: #333333">+</span> msg <span style="color: #333333">+</span> msgSuffix<span style="color: #333333">;</span>
	<span style="color: #333333">};</span>
	
	<span style="color: #555555; font-weight: bold">@Override</span>
	<span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">onApplicationEvent</span><span style="color: #333333">(</span>ApplicationEvent appEvent<span style="color: #333333">)</span> <span style="color: #333333">{</span>
		LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ALC::onApplicationEvent::caching event:: &quot;</span> <span style="color: #333333">+</span> appEvent<span style="color: #333333">);</span>
		LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span> msg<span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;&quot;</span><span style="color: #333333">+</span>appEvent<span style="color: #333333">.</span><span style="color: #0000CC">getClass</span><span style="color: #333333">())</span> <span style="color: #333333">);</span>
        <span style="color: #008800; font-weight: bold">if</span><span style="color: #333333">(</span>appEvent <span style="color: #008800; font-weight: bold">instanceof</span> AuthenticationSuccessEvent <span style="color: #333333">||</span> appEvent <span style="color: #008800; font-weight: bold">instanceof</span>  InteractiveAuthenticationSuccessEvent <span style="color: #333333">)</span>
        <span style="color: #333333">{</span>
            <span style="color: #888888">// Authenticacion success</span>
        	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ALC::onApplicationEvent::AuthenticationSuccessEvent&quot;</span><span style="color: #333333">);</span>
        	AuthenticationSuccessEvent event <span style="color: #333333">=</span> <span style="color: #333333">(</span>AuthenticationSuccessEvent<span style="color: #333333">)</span> appEvent<span style="color: #333333">;</span>
        	UserDetails userDetails <span style="color: #333333">=</span> <span style="color: #333333">(</span>UserDetails<span style="color: #333333">)</span> event<span style="color: #333333">.</span><span style="color: #0000CC">getAuthentication</span><span style="color: #333333">().</span><span style="color: #0000CC">getPrincipal</span><span style="color: #333333">();</span>
        	String msg <span style="color: #333333">=</span> appEvent<span style="color: #333333">.</span><span style="color: #0000CC">getClass</span><span style="color: #333333">()</span> <span style="color: #333333">+</span> <span style="background-color: #fff0f0">&quot; -&gt; &quot;</span> <span style="color: #333333">+</span> userDetails<span style="color: #333333">.</span><span style="color: #0000CC">getUsername</span><span style="color: #333333">();</span>
        	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span> msg<span style="color: #333333">(</span> msg <span style="color: #333333">)</span> <span style="color: #333333">);</span>
        	<span style="color: #888888">// TODO guardar bitacora</span>
        <span style="color: #333333">}</span> 
        <span style="color: #008800; font-weight: bold">else</span> <span style="color: #0066BB; font-weight: bold">if</span> <span style="color: #333333">(</span>appEvent <span style="color: #008800; font-weight: bold">instanceof</span> AbstractAuthenticationFailureEvent<span style="color: #333333">)</span>
        <span style="color: #333333">{</span>
            <span style="color: #888888">// Authentication failure</span>
        	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ALC::onApplicationEvent::AbstractAuthenticationFailureEvent&quot;</span><span style="color: #333333">);</span>
        	<span style="color: #888888">// TODO guardar bitacora</span>
        <span style="color: #333333">}</span>		
        <span style="color: #008800; font-weight: bold">else</span> <span style="color: #0066BB; font-weight: bold">if</span> <span style="color: #333333">(</span>appEvent <span style="color: #008800; font-weight: bold">instanceof</span> SessionDestroyedEvent<span style="color: #333333">)</span>
        <span style="color: #333333">{</span>
            <span style="color: #888888">// Logout from session ( even timeout session )</span>
        	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ALC::onApplicationEvent::SessionDestroyedEvent&quot;</span><span style="color: #333333">);</span>
        	<span style="color: #888888">// TODO guardar bitacora</span>
        <span style="color: #333333">}</span>
        
	<span style="color: #333333">}</span>

	<span style="color: #555555; font-weight: bold">@Override</span>
	<span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">sessionCreated</span><span style="color: #333333">(</span>HttpSessionEvent event<span style="color: #333333">)</span> <span style="color: #333333">{</span>
		<span style="color: #888888">// Create session ( even timeout session )</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ALC::HttpSessionListener::sessionCreated&quot;</span><span style="color: #333333">);</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span> msg<span style="color: #333333">(</span>event<span style="color: #333333">.</span><span style="color: #0000CC">getClass</span><span style="color: #333333">()</span> <span style="color: #333333">+</span> <span style="background-color: #fff0f0">&quot;-&gt;sessionCreated&quot;</span><span style="color: #333333">)</span> <span style="color: #333333">);</span>
    	event<span style="color: #333333">.</span><span style="color: #0000CC">getSession</span><span style="color: #333333">().</span><span style="color: #0000CC">setMaxInactiveInterval</span><span style="color: #333333">(</span><span style="color: #0000DD; font-weight: bold">10</span><span style="color: #333333">*</span><span style="color: #0000DD; font-weight: bold">60</span><span style="color: #333333">);</span>
    	<span style="color: #888888">// TODO guardar bitacora</span>
	<span style="color: #333333">}</span>

	<span style="color: #555555; font-weight: bold">@Override</span>
	<span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">sessionDestroyed</span><span style="color: #333333">(</span>HttpSessionEvent event<span style="color: #333333">)</span> <span style="color: #333333">{</span>
		<span style="color: #888888">// destroy session ( even timeout session )</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ALC::HttpSessionListener::sessionDestroyed&quot;</span><span style="color: #333333">);</span>
    	String msg <span style="color: #333333">=</span> event<span style="color: #333333">.</span><span style="color: #0000CC">getClass</span><span style="color: #333333">()</span> <span style="color: #333333">+</span> <span style="background-color: #fff0f0">&quot; -&gt; sessionDestroyed -&gt; &quot;</span><span style="color: #333333">;</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span> msg<span style="color: #333333">(</span> msg <span style="color: #333333">)</span> <span style="color: #333333">);</span>
    	<span style="color: #888888">// TODO guardar bitacora	</span>
	<span style="color: #333333">}</span>

<span style="color: #333333">}</span>
</pre></td></tr></table>
</div>

		<br/><br/>
		<p>
			<img src="${pageContext.request.contextPath}/resources/images/logging_001.png" style="width: 100%;">
		</p>
		<p>
			<img src="${pageContext.request.contextPath}/resources/images/logging_002.png" style="width: 100%;">
		</p>
		<p>
			<img src="${pageContext.request.contextPath}/resources/images/logging_003.png" style="width: 100%;">
		</p>
		<br/><br/>


		
</nav>
