<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A3 - 2017 Sensitive Data Exposure</h1>

		<hr/>
		<h2>Exposici�n de Datos Sensibles</h2>
		<p>Muchas aplicaciones web no protegen adecuadamente los datos sensibles,
			tales como tarjetas de cr�dito, identificadores de impuestos y credenciales de autenticaci�n.</p>
		<p>Los atacantes pueden robar o modificar los datos debilmente protegidos para llevar a cabo un fraude
			con tarjeta de cr�dito, robo de identidad u otros cr�menes</p>
		<p>Los datos sensibles merecen protecci�n extra tales como cifrado al almacenarse o en tr�nsito,
			as� como precauciones especiales cuando se intercambian con el navegador.</p>

		<h2><hr/>Spring Security</h2>
		<p>El m�dulo de criptograf�a de Spring Security proporciona las capacidades de criptograf�a necesarias.</p>
		
		<div class="square">
			<h2>Spring Security en servicio REST</h2>
		  	<p>En la implementaci�n de autenticaci&oacute;n b&aacute;sica proporciona los componentes para valida
		  		 el header <i>Authorizati�n : Basic user_64:password_64</i></p>
		  	<p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
GatewayClient.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> xpd<span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">client</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">GatewayClient</span> <span style="color: #333333">{</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #008800; font-weight: bold">public</span> String <span style="color: #0066BB; font-weight: bold">personCall</span><span style="color: #333333">(</span> String user<span style="color: #333333">,</span> String token<span style="color: #333333">,</span> OperationType operation<span style="color: #333333">,</span> String requestJson <span style="color: #333333">){</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>	
		String plainCredentials <span style="color: #333333">=</span> user <span style="color: #333333">+</span> <span style="background-color: #fff0f0">&quot;:&quot;</span> <span style="color: #333333">+</span> token<span style="color: #333333">;</span>
		String base64Credentials <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> String<span style="color: #333333">(</span>Base64<span style="color: #333333">.</span><span style="color: #0000CC">getEncoder</span><span style="color: #333333">().</span><span style="color: #0000CC">encodeToString</span><span style="color: #333333">(</span>plainCredentials<span style="color: #333333">.</span><span style="color: #0000CC">getBytes</span><span style="color: #333333">()));</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
		HttpHeaders headers <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> HttpHeaders<span style="color: #333333">();</span>
		headers<span style="color: #333333">.</span><span style="color: #0000CC">set</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;Authorization&quot;</span><span style="color: #333333">,</span> <span style="background-color: #fff0f0">&quot;Basic &quot;</span> <span style="color: #333333">+</span> base64Credentials<span style="color: #333333">);</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #333333">}</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
			</p>
		</div>


		<div class="square">
			<h2>Spring Security en aplicaciones web</h2>
		  	<p>En las aplicaciones web, Spring Security proporciona implementaciones para el uso de codificadores de contrase�as:</p>
		  	<p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
SecurityConfig.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Configuration</span>
<span style="color: #555555; font-weight: bold">@EnableWebSecurity</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">SecurityConfig</span> <span style="color: #008800; font-weight: bold">extends</span> WebSecurityConfigurerAdapter <span style="color: #333333">{</span>

        <span style="color: #555555; font-weight: bold">@Autowired</span>
        DataSource dataSource<span style="color: #333333">;</span>

        <span style="color: #555555; font-weight: bold">@Autowired</span>
        <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">configAuthentication</span><span style="color: #333333">(</span>AuthenticationManagerBuilder auth<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> Exception <span style="color: #333333">{</span>
                auth<span style="color: #333333">.</span><span style="color: #0000CC">jdbcAuthentication</span><span style="color: #333333">().</span><span style="color: #0000CC">dataSource</span><span style="color: #333333">(</span>dataSource<span style="color: #333333">)</span>
                        <span style="color: #333333">.</span><span style="color: #0000CC">passwordEncoder</span><span style="color: #333333">(</span>passwordEncoder<span style="color: #333333">())</span>
                        <span style="color: #333333">.</span><span style="color: #0000CC">usersByUsernameQuery</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;sql...&quot;</span><span style="color: #333333">)</span>
                        <span style="color: #333333">.</span><span style="color: #0000CC">authoritiesByUsernameQuery</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;sql...&quot;</span><span style="color: #333333">);</span>
        <span style="color: #333333">}</span>

        <span style="color: #555555; font-weight: bold">@Bean</span>
        <span style="color: #008800; font-weight: bold">public</span> PasswordEncoder <span style="color: #0066BB; font-weight: bold">passwordEncoder</span><span style="color: #333333">(){</span>
                PasswordEncoder encoder <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> BCryptPasswordEncoder<span style="color: #333333">();</span>
                <span style="color: #008800; font-weight: bold">return</span> encoder<span style="color: #333333">;</span>
        <span style="color: #333333">}</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
			</p>
		</div>


</nav>
