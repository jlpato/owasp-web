<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A2 - 2017 Broken Authentication and Session Management</h1>

		<hr/>
		<h2>P&eacute;rdida de Autenticación y Administración de la sesión</h2>
		<p>Las funciones de la aplicaci&oacute;n relacionadas a autenticaci&oacute;n y gesti&oacute;n de sesiones son
		implementadas incorrectamente, permitiendo a los atacantes comprometer usuarios y
		contrase&ntilde;as, token de sesiones, o explotar otras fallas de implementaci&oacute;n para asumir la
		identidad de otros usuarios (temporal o permanentemente).
		</p>
		<p>Proporciona mecanismos para la administraci&oacute;n de sesi&oacute;n y autenticaci&oacute;n segura y eficiente.</p>

		<div class="square">
			<h2>Spring Security</h2>
		  <p>

		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">pacrdd</span><span style="color: #333333">.</span><span style="color: #0000CC">spring</span><span style="color: #333333">.</span><span style="color: #0000CC">config</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #555555; font-weight: bold">@Configuration</span>
<span style="color: #555555; font-weight: bold">@EnableWebSecurity</span>
<span style="color: #555555; font-weight: bold">@EnableGlobalMethodSecurity</span><span style="color: #333333">(</span>prePostEnabled<span style="color: #333333">=</span><span style="color: #008800; font-weight: bold">true</span><span style="color: #333333">)</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">SecurityConfig</span> <span style="color: #008800; font-weight: bold">extends</span> WebSecurityConfigurerAdapter <span style="color: #333333">{</span>
    <span style="color: #333333">...</span>
    <span style="color: #008800; font-weight: bold">public</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">configureGlobalSecurity</span><span style="color: #333333">(</span>AuthenticationManagerBuilder auth<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> Exception <span style="color: #333333">{</span>
    	auth<span style="color: #333333">.</span><span style="color: #0000CC">jdbcAuthentication</span><span style="color: #333333">()</span>
    		<span style="color: #333333">.</span><span style="color: #0000CC">usersByUsernameQuery</span><span style="color: #333333">(</span>QUERY_FOR_USERS<span style="color: #333333">)</span>
    		<span style="color: #333333">.</span><span style="color: #0000CC">authoritiesByUsernameQuery</span><span style="color: #333333">(</span>QUERY_FOR_ROLES<span style="color: #333333">)</span>
    		<span style="color: #333333">.</span><span style="color: #0000CC">dataSource</span><span style="color: #333333">(</span>dataSource<span style="color: #333333">);</span>
    <span style="color: #333333">}</span>
    <span style="color: #FF0000; background-color: #FFAAAA">...</span>
    <span style="color: #008800; font-weight: bold">protected</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">configure</span><span style="color: #333333">(</span>HttpSecurity http<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> Exception <span style="color: #333333">{</span>
    	http
    		<span style="color: #333333">.</span><span style="color: #0000CC">authorizeRequests</span><span style="color: #333333">()</span>
    		<span style="color: #333333">.</span><span style="color: #0000CC">antMatchers</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/resources/**&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">permitAll</span><span style="color: #333333">()</span>
	    	<span style="color: #333333">.</span><span style="color: #0000CC">antMatchers</span><span style="color: #333333">(</span>loginPage<span style="color: #333333">).</span><span style="color: #0000CC">permitAll</span><span style="color: #333333">()</span>
	    	<span style="color: #333333">.</span><span style="color: #0000CC">antMatchers</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/**&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">access</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;isAuthenticated()&quot;</span><span style="color: #333333">)</span>
	    	<span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">formLogin</span><span style="color: #333333">().</span><span style="color: #0000CC">loginPage</span><span style="color: #333333">(</span>loginPage<span style="color: #333333">)</span>
	    		<span style="color: #333333">.</span><span style="color: #0000CC">usernameParameter</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;username&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">passwordParameter</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;password&quot;</span><span style="color: #333333">)</span>
	    		<span style="color: #333333">.</span><span style="color: #0000CC">failureUrl</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/login.jsp?error=true&quot;</span><span style="color: #333333">)</span>
	            <span style="color: #333333">.</span><span style="color: #0000CC">loginProcessingUrl</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/login&quot;</span><span style="color: #333333">)</span>
            <span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">logout</span><span style="color: #333333">()</span>
            	<span style="color: #333333">.</span><span style="color: #0000CC">logoutUrl</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/logout&quot;</span><span style="color: #333333">)</span>
            	<span style="color: #333333">.</span><span style="color: #0000CC">clearAuthentication</span><span style="color: #333333">(</span><span style="color: #008800; font-weight: bold">true</span><span style="color: #333333">)</span>
            	<span style="color: #333333">.</span><span style="color: #0000CC">invalidateHttpSession</span><span style="color: #333333">(</span><span style="color: #008800; font-weight: bold">true</span><span style="color: #333333">)</span>
            	<span style="color: #333333">.</span><span style="color: #0000CC">logoutSuccessUrl</span><span style="color: #333333">(</span>loginPage<span style="color: #333333">)</span>
            	<span style="color: #333333">.</span><span style="color: #0000CC">permitAll</span><span style="color: #333333">()</span>
            <span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">sessionManagement</span><span style="color: #333333">()</span>
            	<span style="color: #333333">.</span><span style="color: #0000CC">sessionCreationPolicy</span><span style="color: #333333">(</span>SessionCreationPolicy<span style="color: #333333">.</span><span style="color: #0000CC">ALWAYS</span><span style="color: #333333">);</span>
    <span style="color: #333333">}</span>
    <span style="color: #333333">...</span> 
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
		</div>


</nav>
