<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<style>
<!--
table.requests{
	border: black;
	border-style: groove;
}
td.requests{
	 border: black;
	 border-style: groove;
	 width: 40%;
	 padding: 3px;
	 vertical-align: top;
}
-->
</style>
<nav>
    	
    	<h1>A4 - 2017 XML External Entities (XXE)</h1>

		<hr/>
		<h2>Entidades Externas XML (XXE)</h2>
		<p>Muchos procesadores XML antiguos o mal configurados eval&uacute;an referencias a entidades
		externas en documentos XML. Las entidades externas pueden utilizarse para revelar archivos
		internos mediante la URI o archivos internos en servidores no actualizados, escanear puertos de
		la LAN, ejecutar c�digo de forma remota y realizar ataques de denegaci&oacute;n de servicio (DoS).
		</p>
		
		<h2>&iquest;Qu&eacute; es una XML External Entity (XXE)?</h2>
		<div>
			<table cellspacing="0" cellpadding="5" border="1" bgcolor="#cccccc">
				<tbody>
					<tr>
						<td> <pre>&lt;!ENTITY name "entity_value"&gt; </pre> </td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<p>donde <code>entity value</code> es cualquier caracter que no es un '&amp;', '%' or ' " ',
		una referencia de entidad de par&aacute;metro ('%Name;'), una referencia de entidad ('&amp;Name;')
		o una referencia de caracter del glosario Unicode.</p>
		
		<div>
			<table width="100%" cellspacing="0" cellpadding="5" border="1">
				<tbody>
					<tr>
						<th bgcolor="#eeeeee" align="left"><span class="main_text_bold_blue">Ejemplo:</span> </th>
					</tr>
					<tr>
						<td><pre>&lt;?xml version="1.0" standalone="yes" ?&gt;
&lt;!DOCTYPE author [
  &lt;!ELEMENT author (#PCDATA)&gt;
  &lt;!ENTITY js "Jo Smith"&gt;
]&gt;
&lt;author&gt;&amp;js;&lt;/author&gt;
</pre></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div>
			<table width="100%" cellspacing="0" cellpadding="5" border="1">
				<tbody>
					<tr>
						<th bgcolor="#eeeeee" align="left"><span class="main_text_bold_blue">Ejemplo:</span> </th>
					</tr>
					<tr>
						<td><pre>&lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;</b>
 &lt;!DOCTYPE foo [  
   &lt;!ELEMENT foo ANY &gt;
   &lt;!ENTITY xxe SYSTEM "file:///etc/passwd" &gt;]&gt;
 &lt;foo&gt;&amp;xxe;&lt;/foo&gt;
</pre></td>
					</tr>
				</tbody>
			</table>
		</div>

		<!-- http://hilite.me/ -->
		
		<!-- https://backtrackacademy.com/articulo/explorando-la-vulnerabilidad-xxe-xml-external-entity -->
		<h2>&iquest;Qu&eacute; es la vulnerabilidad de XXE y en qu&eacute; consiste?</h2>
		
		<p>XXE se refiere a un ataque de falsificaci&oacute;n de solicitud de servidor (SSRF), mediante el cual un atacante es capaz de causar:</p>
		<ol>
			<li>Denegaci&oacute;n de servicio (DDoS)</li>
			<li>Acceso a archivos y servicios locales o remotos</li>
		</ol>
		<p>Una vulnerabilidad de XXE consiste en una inyecci&oacute;n que se aprovecha de la mala configuraci�n del int&eacute;rprete XML
		permitiendo incluir entidades externas, este ataque se realiza contra una aplicaci�n que interpreta lenguaje XML en sus par&aacute;metros.</p>		
    
		<h3>&iquest;C&oacute;mo se detecta esta vulnerabilidad?</h3>

		<p>La siguiente imagen nos muestra un <code>request</code> realizado mediante el m�todo <code>POST</code>:</p>
		
		<p align="center">
			<img src="${pageContext.request.contextPath}/resources/images/xxe_001.png" style="width: 100%;">
			<span>Content Types = XML</span>
		</p>

		<p>Como podemos visualizar en la imagen anterior el <code>Content Types</code> es de tipo <code>text/xml</code>, <code>application/xml</code>
		estos son indicadores que nos permiten verificar, c&oacute;mo los par&aacute;metros son enviados por el request a un int&eacute;rprete de XML,
		adicionalmente tambi&eacute;n puede darse al parsear archivos como JSON a XML e inyectar el payload en el sitio web.</p>

		<h4>Denegaci&oacute;n de Servicio</h4>
		
		<p>Veamos el ejemplo a continuaci&oacute;n</p>

		<p>Mediante las etiquetas <code>&lt;foo&gt;&lt;/foo&gt;</code> podremos obtener como resultado el String o variable que configuremos</p>

		<div align="center">
			<table class="requests">
			  <tr>
			    <td class="requests">
					<code>POST Prueba.com/Interprete_XML  HTTP/1.1</code>
					<br/><code>&lt;foo&gt;Primer Ejemplo&lt;/foo&gt;</code>
			    </td>
			    <td class="requests">
					<code>HTTP/1.0 200 OK</code>
					<br/><code>Primer Ejemplo</code>
			    </td>
			  </tr>
			</table>
			<span>Interprete XML</span>
		</div>

		<p>Podemos dise&ntilde;ar una funci&oacute;n que repita una variable, que posteriormente el resultado de la variable se vaya anidando en otra,
		en otra  y en otra, as&iacute; sucesivamente:</p>
 
		<div align="center">
			<table class="requests">
			  <tr>
			    <td class="requests">
					<code>POST Prueba.com/Interprete_XML  HTTP/1.1</code>
					<br/><code>&lt;DOCTYPE foo [</code>
					<br/><code>&lt;ELEMENT foo ANY&gt;</code>
					<br/><code>&lt;ENTITY variable "Segundo Ejemplo"&gt;</code>
					<br/><code>&lt;ENTITY var1 "&amp;variable;&amp;variable;"&gt;</code>
					<br/><code>&lt;ENTITY var2 "&amp;var1;&amp;var1;&amp;var1;&amp;var1;"&gt;</code>
					<br/><code>&lt;ENTITY var3 "&amp;var2;&amp;var2;&amp;var2;&amp;var2;&amp;var2;"&gt;</code>
					<br/><code>&lt;ELEMENT foo ANY&gt;</code>
					<br/><code>]&gt;</code>
					<br/><code>&lt;foo&gt;</code>
					<br/><code>&amp;var3;</code>
					<br/><code>&lt;/foo&gt;</code>
			    </td>
			    <td class="requests">
					<code>HTTP/1.0 200 OK</code>
					<br/><code>
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
						Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo Segundo Ejemplo
					</code>
			    </td>
			  </tr>
			</table>
			<span>Int&eacute;rprete XML peticiones masivas "ataque denegaci&oacute;n de servicios"</span>
		</div>

		<h4>Inyecci&oacute;n</h4>
		
		<p>Para este ejemplo podemos observar c&oacute;mo se est&aacute; enviado una etiqueta de texto
		mediante el m&eacute;todo <code>GET</code> a un int&eacute;rprete de <code>XML</code></p>
		
		<p align="center">
			<img src="${pageContext.request.contextPath}/resources/images/xxe_002.png" style="width: 100%;">
			<span>Int&eacute;rprete XML mediante m&eacute;todo <code>GET</code></span>
		</p>
		
		<p>En base a esta informaci&oacute;n crearemos el siguiente Payload de manera que enviaremos al sistema una petici&oacute;n
		que nos devuelva el contenido de un archivo en espec&iacute;fico</p>
		
		<p><code>GET  URL/xml/example.php?xml=&lt;test&gt;Hacker&lt;/test&gt;</code></p>

		<p>Para comprobar que la instancia es vulnerable cambiaremos la etiqueta &lt;test&gt;
			y utilizaremos el siguiente payload basado en el segundo ejemplo:</p>
		
		<p><code> &lt;!DOCTYPE foo [ &lt;!ELEMENT foo ANY&gt; &lt;!ENTITY bar "World"&gt;  &lt;!ENTITY barxee "&amp;bar; XEE" &gt; ]&gt; &lt;foo&gt; &amp;barxee; &lt;/foo&gt; </code></p>

		<p>A continuaci&oacute;n podemos visualizar el resultado:</p>

		<p align="center">
			<img src="${pageContext.request.contextPath}/resources/images/xxe_003.png" style="width: 100%;">
			<span>URL vulnerable a <code>XXE</code></span>
		</p>

		<p>Como podemos evidenciar la petici&oacute;n fue procesada de manera exitosa.</p>

		<p>Basados en esta evidencia volveremos a manipular el payload de modo que nos permitir&aacute; obtener informaci&oacute;n
			cr&iacute;tica del sistema.</p>
		
		<p>La siguiente petici&oacute;n solicitar&aacute; al servidor la lista de usuarios del archivo <code>/etc/passwd</code></p>

		<p><code>&lt;!DOCTYPE foo  [&lt;!ENTITY bar SYSTEM "file:///etc/passwd"&gt;]&gt; &lt;foo&gt;&bar;&lt;/foo&gt;</code></p>

		<p align="center">
			<img src="${pageContext.request.contextPath}/resources/images/xxe_004.png" style="width: 100%;">
			<span>Responde con la lista de usuarios del servidor.</span>
		</p>
		




		<br/><br/>
		<h2>Referencias</h2>
		<br/><a target="_blank" href="https://stackoverflow.com/questions/1544200/what-is-difference-between-xml-schema-and-dtd">Stackoverflow: DTD vs XSD</a>
		<br/><a target="_blank" href="https://backtrackacademy.com/articulo/explorando-la-vulnerabilidad-xxe-xml-external-entity">https://backtrackacademy.com/</a>
		<br/><a target="_blank" href="https://www.w3.org/TR/xml-entity-names/">https://www.w3.org/TR/xml-entity-names/</a>
		<br/><a target="_blank" href="http://www.w3.org/2003/entities/2007xml/entities.xsl">http://www.w3.org/2003/entities/2007xml/entities.xsl</a>
		<br/><a target="_blank" href="https://www.xml.com/pub/a/98/08/xmlqna2.html">https://www.xml.com/pub/a/98/08/xmlqna2.html</a>
		<br/><a target="_blank" href="https://xmlwriter.net/xml_guide/entity_declaration.shtml">https://xmlwriter.net/xml_guide/entity_declaration.shtml</a>
		
</nav>
