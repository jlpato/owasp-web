<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav>
    	
    	<h1>A8 - Cross-Site Request Forgery (CSRF)</h1>

		<h2><hr/>Falsificaci�n de peticiones</h2>
		<p>CSRF es un ataque que forza al usuario a ejecutar acciones no deseadas en una aplicaci�n web en la cu�l esta actualmente autenticado.</p>
		<p>Los ataques CSRF se dirigen espec�ficamente a solicitudes que cambian el estado, no al robo de datos,
			ya que el atacante no tiene forma de ver la respuesta a la solicitud falsificada</p>
		<p>Con un poco de ayuda de ingenier�a social (como enviar un enlace por correo electr�nico o chat),
			un atacante puede enga�ar a los usuarios de una aplicaci�n web para que ejecuten las acciones que el atacante elija.</p>
		<p>Si la v�ctima es un usuario normal, un ataque CSRF exitoso puede obligar al usuario a realizar peticiones de cambio de estado,
			como transferir fondos, cambiar su direcci�n de correo electr�nico, etc.</p>
		<p>Si la v�ctima es una cuenta administrativa, CSRF puede comprometer toda la aplicaci�n web.</p>
		

		<h2><hr/>Spring Security</h2>
		<p>Proporciona el soporte para la generaci�n y validaci�n de tokens mitigando ataques de peticiones cruzadas.</p>

		<div class="square">
			<h2>CSRF en servicio REST</h2>
			<p>Se implementa el env�o de un token como atributo en el header y como cookie, en las peticiones hacia el gateway.</p>
		  <p>

		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
SecurityConfig.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">spring</span><span style="color: #333333">.</span><span style="color: #0000CC">config</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #555555; font-weight: bold">@Configuration</span>
<span style="color: #555555; font-weight: bold">@EnableWebSecurity</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">SecurityConfig</span> <span style="color: #008800; font-weight: bold">extends</span> WebSecurityConfigurerAdapter <span style="color: #333333">{</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">protected</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">configure</span><span style="color: #333333">(</span>HttpSecurity http<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> Exception <span style="color: #333333">{</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;configure(HttpSecurity http)&quot;</span><span style="color: #333333">);</span>
    	http<span style="color: #333333">.</span><span style="color: #0000CC">csrf</span><span style="color: #333333">().</span><span style="color: #0000CC">csrfTokenRepository</span><span style="color: #333333">(</span>CookieCsrfTokenRepository<span style="color: #333333">.</span><span style="color: #0000CC">withHttpOnlyFalse</span><span style="color: #333333">());</span>
    	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
    <span style="color: #333333">}</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
			
			<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
GatewayClient.java
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> xpd<span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">client</span><span style="color: #333333">;</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">GatewayClient</span> <span style="color: #333333">{</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #008800; font-weight: bold">public</span> String <span style="color: #0066BB; font-weight: bold">personCall</span><span style="color: #333333">(</span> String user<span style="color: #333333">,</span> String token<span style="color: #333333">,</span> OperationType operation<span style="color: #333333">,</span> String requestJson <span style="color: #333333">){</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
		CsrfTokenRepository csrfTokenRepository <span style="color: #333333">=</span> CookieCsrfTokenRepository<span style="color: #333333">.</span><span style="color: #0000CC">withHttpOnlyFalse</span><span style="color: #333333">();</span>
		CsrfToken csrfToken <span style="color: #333333">=</span> csrfTokenRepository<span style="color: #333333">.</span><span style="color: #0000CC">generateToken</span><span style="color: #333333">(</span><span style="color: #008800; font-weight: bold">null</span><span style="color: #333333">);</span>
		
		HttpHeaders headers <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> HttpHeaders<span style="color: #333333">();</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
		headers<span style="color: #333333">.</span><span style="color: #0000CC">add</span><span style="color: #333333">(</span>csrfToken<span style="color: #333333">.</span><span style="color: #0000CC">getHeaderName</span><span style="color: #333333">(),</span> csrfToken<span style="color: #333333">.</span><span style="color: #0000CC">getToken</span><span style="color: #333333">());</span>
		headers<span style="color: #333333">.</span><span style="color: #0000CC">add</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;Cookie&quot;</span><span style="color: #333333">,</span> <span style="background-color: #fff0f0">&quot;XSRF-TOKEN=&quot;</span> <span style="color: #333333">+</span> csrfToken<span style="color: #333333">.</span><span style="color: #0000CC">getToken</span><span style="color: #333333">());</span>
		<span style="color: #FF0000; background-color: #FFAAAA">...</span>
	<span style="color: #333333">}</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->

		</div>


		<div class="square">
			<h2>CSRF en servicio aplicaci�n web</h2>
			<p>En las aplicaciones web tambi�n se implementa el env�o de un token para evitar ataques de referencias cruzadas. El token se env�a como un campo oculto dentro de un formulario (petici�n v�a m�todo POST)
			o como un argumento en la url (petici�n v�a m�todo GET)</p>
			<p>A partir de las versiones 4.0 de Spring Security, la protecci�n CSRF est� habilitada por omisi�n,
			con el prop�sito de deshabilitarlo expl�citamente en caso de no requerirlo.
			De tal modo que no se requiere aplicar ninguna configuraci�n adicional para poder implementar el uso del env�o del <i>token</i></p>
			<p>Para su implementaci�n, en las peticiones POST el token se env�a como parte del formulario (form data):</p>
		  <p>

		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
login.jsp
<table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">&lt;%@</span> page language<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;java&quot;</span> contentType<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;text/html; charset=ISO-8859-1&quot;</span>
    pageEncoding<span style="color: #333333">=</span><span style="background-color: #fff0f0">&quot;ISO-8859-1&quot;</span><span style="color: #008800; font-weight: bold">%&gt;</span>
...
<span style="color: #007700">&lt;html</span> <span style="color: #0000CC">xmlns:c=</span><span style="background-color: #fff0f0">&quot;http://java.sun.com/jsp/jstl/core&quot;</span><span style="color: #007700">&gt;</span>
<span style="color: #007700">&lt;head&gt;</span>
...
<span style="color: #007700">&lt;/head&gt;</span>
<span style="color: #007700">&lt;body&gt;</span>
...
		  <span style="color: #007700">&lt;form</span> <span style="color: #0000CC">action=</span><span style="background-color: #fff0f0">&quot;./login&quot;</span> <span style="color: #0000CC">method=</span><span style="background-color: #fff0f0">&quot;post&quot;</span><span style="color: #007700">&gt;</span>
		  <span style="color: #007700">&lt;input</span> <span style="color: #0000CC">type=</span><span style="background-color: #fff0f0">&quot;hidden&quot;</span> <span style="color: #0000CC">name=</span><span style="background-color: #fff0f0">&quot;\${_csrf.parameterName}&quot;</span> <span style="color: #0000CC">value=</span><span style="background-color: #fff0f0">&quot;\${_csrf.token}&quot;</span> <span style="color: #007700">/&gt;</span>
...
		  <span style="color: #007700">&lt;/form&gt;</span>
...
<span style="color: #007700">&lt;/body&gt;</span>
<span style="color: #007700">&lt;/html&gt;</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
		</div>

</nav>
