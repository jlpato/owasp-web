<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A4 - Insecure Direct Object References</h1>

		<h2><hr/>Referencias inseguras directas de objetos en servicio REST</h2>
		<p>En el gateway se realiza la autenticación y autorización en cada petición debido a la naturaleza de un servicio REST,
			y cada recurso puede permitirse únicamente al rol o usuario definido para consumirlo.</p>

		<div class="square">
			<h2>Spring Security</h2>
		  <p>
		  	<!-- HTML generated using hilite.me -->
<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #008800; font-weight: bold">package</span> com<span style="color: #333333">.</span><span style="color: #0000CC">xpd</span><span style="color: #333333">.</span><span style="color: #0000CC">web</span><span style="color: #333333">.</span><span style="color: #0000CC">gateway</span><span style="color: #333333">.</span><span style="color: #0000CC">spring</span><span style="color: #333333">.</span><span style="color: #0000CC">config</span><span style="color: #333333">;</span>
<span style="color: #333333">...</span>
<span style="color: #555555; font-weight: bold">@Configuration</span>
<span style="color: #555555; font-weight: bold">@EnableWebSecurity</span>
<span style="color: #008800; font-weight: bold">public</span> <span style="color: #008800; font-weight: bold">class</span> <span style="color: #BB0066; font-weight: bold">SecurityConfig</span> <span style="color: #008800; font-weight: bold">extends</span> WebSecurityConfigurerAdapter <span style="color: #333333">{</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
    <span style="color: #555555; font-weight: bold">@Override</span>
    <span style="color: #008800; font-weight: bold">protected</span> <span style="color: #333399; font-weight: bold">void</span> <span style="color: #0066BB; font-weight: bold">configure</span><span style="color: #333333">(</span>HttpSecurity http<span style="color: #333333">)</span> <span style="color: #008800; font-weight: bold">throws</span> Exception <span style="color: #333333">{</span>
    	LOGGER<span style="color: #333333">.</span><span style="color: #0000CC">debug</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;configure(HttpSecurity http)&quot;</span><span style="color: #333333">);</span>
	<span style="color: #FF0000; background-color: #FFAAAA">...</span>
    	http<span style="color: #333333">.</span><span style="color: #0000CC">authorizeRequests</span><span style="color: #333333">()</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">antMatchers</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;/person/**&quot;</span><span style="color: #333333">).</span><span style="color: #0000CC">hasRole</span><span style="color: #333333">(</span><span style="background-color: #fff0f0">&quot;ADMIN&quot;</span><span style="color: #333333">)</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">httpBasic</span><span style="color: #333333">().</span><span style="color: #0000CC">realmName</span><span style="color: #333333">(</span>Constants<span style="color: #333333">.</span><span style="color: #0000CC">SECURITY_REALM</span><span style="color: #333333">)</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">authenticationEntryPoint</span><span style="color: #333333">(</span>getBasicAuthEntryPoint<span style="color: #333333">())</span>
	        <span style="color: #333333">.</span><span style="color: #0000CC">and</span><span style="color: #333333">().</span><span style="color: #0000CC">sessionManagement</span><span style="color: #333333">().</span><span style="color: #0000CC">sessionCreationPolicy</span><span style="color: #333333">(</span>SessionCreationPolicy<span style="color: #333333">.</span><span style="color: #0000CC">STATELESS</span><span style="color: #333333">);</span>
    <span style="color: #333333">}</span>
<span style="color: #FF0000; background-color: #FFAAAA">...</span>
<span style="color: #333333">}</span>
</pre></td></tr></table></div>

			<!-- HTML generated using hilite.me -->
		</div>


		<h2><hr/>Referencias inseguras directas de objetos en la aplicación web</h2>
		<p>En las aplicaciones web se realiza la autenticación y posteriormente la autorización en cada petición,
		se debe implementar el uso de un Mapa de Referencias de Accesos para urls que lo requieran.</p>
		<p>Se deben evitar urls como la siguiente:</p>
		<img alt="insecure direct object reference" src="${pageContext.request.contextPath}/resources/images/bad_url.png"/>

		<div class="square">
			<h2>ESAPI</h2>
		  <p>
		  	<!-- HTML generated using hilite.me -->
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17</pre></td><td><pre style="margin: 0; line-height: 125%">MyObject obj<span style="color: #333333">;</span>	 	<span style="color: #888888">// generate your object</span>
Collection coll<span style="color: #333333">;</span> 	<span style="color: #888888">// holds objects for display in UI</span>

<span style="color: #888888">//create ESAPI random access reference map</span>
AccessReferenceMap map <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> RandomAccessReferenceMap<span style="color: #333333">();</span>

<span style="color: #888888">//get indirect reference using direct reference as seed input</span>
String indirectReference <span style="color: #333333">=</span> map<span style="color: #333333">.</span><span style="color: #0000CC">addDirectReference</span><span style="color: #333333">(</span>obj<span style="color: #333333">.</span><span style="color: #0000CC">getId</span><span style="color: #333333">());</span>

<span style="color: #888888">//set indirect reference for each object - requires your app object to have this method</span>
obj<span style="color: #333333">.</span><span style="color: #0000CC">setIndirectReference</span><span style="color: #333333">(</span>indirectReference<span style="color: #333333">);</span>

<span style="color: #888888">//add object to display collection</span>
coll<span style="color: #333333">.</span><span style="color: #0000CC">add</span><span style="color: #333333">(</span>obj<span style="color: #333333">);</span>

<span style="color: #888888">//store collection in request/session and forward to UI</span>
<span style="color: #333333">...</span>
</pre></td></tr></table></div>
			<!-- HTML generated using hilite.me -->
		</div>

</nav>
