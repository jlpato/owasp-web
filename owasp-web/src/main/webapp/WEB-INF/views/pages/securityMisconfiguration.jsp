<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A5 - Security Misconfiguration</h1>

		<h2><hr/>Configuraci�n Incorrecta de Seguridad</h2>
		<p>La configuraci�n incorrecta de seguridad es simplemente eso: ensamblar incorrectamente las protecciones para una aplicaci�n web.
			Estas configuraciones err�neas suelen ocurrir cuando los administradores de sistemas, DBA o desarrolladores
			dejan huecos en el marco de seguridad de una aplicaci�n.
			Pueden ocurrir en cualquier nivel del stack de las aplicaciones, incluida la plataforma, el servidor de aplicaciones,
			la base de datos, los frameworks y el c�digo personalizado.
			Estas configuraciones err�neas de seguridad pueden llevar al atacante directamente al sistema
			y resultar en un sistema parcial o incluso totalmente comprometido.</p>
		<p>Los atacantes encuentran estas configuraciones incorrectas a trav�s del acceso no autorizado a cuentas predeterminadas,
			p�ginas web no utilizadas, fallas no reparadas, archivos y directorios no protegidos y m�s.
			Si un sistema se ve comprometido por configuraciones de seguridad defectuosas,
			los datos pueden ser robados o modificados lentamente con el tiempo y pueden requerir mucho tiempo y costosas recuperaciones.</p>
		<p>Es importante que todo el stack de la aplicaci�n web carezca de vulnerabilidades.
			A diferencia de muchos de los 10 principales riesgos de OWASP, los desarrolladores no son los �nicos responsables de
			prevenir fallas de configuraci�n incorrecta de seguridad.
			Los desarrolladores deben colaborar con los administradores para garantizar que todo el stack est� configurado correctamente.</p>
		<p>Las configuraciones incorrectas de seguridad son f�ciles de explotar, pero hay una serie de formas proactivas para prevenirlas,
			incluidas las siguientes recomendaciones de expertos de la industria:

			<ol>
			    <li>Desarrollar un proceso repetible para reducir el stack de la vulnerabilidad</li>
			    <li>Deshabilitar cuentas predeterminadas y cambiar contrase�as</li>
			    <li>Mantener el software actualizado</li>
			    <li>Desarrollar una arquitectura de aplicaciones s�lida que a�sle de manera efectiva los componentes y cifre los datos que son especialmente importantes con los datos confidenciales.</li>
			    <li>Deshabilitar cualquier archivo o caracter�stica innecesarios</li>
			    <li>Aseg�rarse de que las configuraciones de seguridad en los frameworks de desarrollo y las bibliotecas est�n configuradas en valores seguros</li>
			    <li>Ejecutar herramientas (es decir, esc�neres autom�ticos) y realizar auditor�as peri�dicas para identificar agujeros en la configuraci�n de seguridad</li>
			</ol>

		<p>Las aplicaciones web son mucho m�s complejas hoy que en el pasado.
			Tienen numerosas capas que aumentan el stack para un posible ataque.
			Durante el proceso de desarrollo, as� como la implementaci�n y el uso y mantenimiento constantes de la aplicaci�n web,
			es imperativo que se tomen las medidas de seguridad adecuadas para reducir cualquier posible punto de explotaci�n.
			Garantizar que la configuraci�n de seguridad est� configurada correctamente y se verifique con frecuencia
			es fundamental para proteger los activos de informaci�n de una organizaci�n.</p></p>
		


		<div class="square">
			<h2>No son tareas del desarrollador</h2>
		  	<p>
		  		<ol>
		  			<li>Actualizaciones para la aplicaci�n, el servidor de aplicaciones, la base de datos, sistema operativo, etc.</li>
		  			<li>Configuraci�n de la aplicaci�n, servidor de apkicaciones, la base de datos, cortafuegos, permisos de usuario.</li>
  					<li>Inhabilitar las funcionalidades innecesarias, inhabilitar puertos, servicios.</li>
	  			</ol>
  			</p>
		</div>

		<div class="square">
			<h2>Tareas del desarrollador</h2>
		  	<p>
		  		<ol>
		  			<li>Configurar el logging, el manejo de las excepciones.</li>
		  			<li>
		  				<ol>
		  					<li>Evitar los errores t�cnicos en la Vista</li>
		  					<li>Nunca tratar el log sobre una aplicaci�n en un ambiente productivo.</li>
		  				</ol>
		  			</li>
		  			<li>Configuraci�n del framework de Seguridad.</li>
		  			<li>
		  				<ol>
		  					<li>Configuraciones relacionadas a la Seguridad en todos ls frameworks usados.</li>
		  					<li>Actualizaciones de Seguridad, nuevas versiones de librerias.</li>
		  				</ol>
		  			</li>
	  			</ol>
  			</p>
		</div>

</nav>
