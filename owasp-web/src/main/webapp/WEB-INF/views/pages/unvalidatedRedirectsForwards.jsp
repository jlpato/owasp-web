<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A10 - Unvalidated Redirects and Forwards</h1>

		<h2><hr/>Autenticaci�n rota y Administraci�n de la sesi�n</h2>
		<p>Las aplicaciones web frecuentemente redirigen y env�an a los usuarios a otras p�ginas y sitios web,
			y utilizan datos que no son de confianza para determinar las p�ginas de destino.
			Sin la validaci�n adecuada, los atacantes pueden redirigir a las v�ctimas a sitios de phishing o malware,
			o usar reenvios para acceder a p�ginas no autorizadas. </p>

		<div class="square">
			<h2>�C�mo evito los redireccionamientos y reenv�os no validados?</h2>
		  	<p>El uso seguro de los redireccionamientos y reenv�os se puede hacer de varias maneras:
		  		<ol>
		  			<li>Simplemente evite el uso de redireccionamientos y reenv�os.</li>
		  			<li>Si se usa, no involucre par�metros de usuario al calcular el destino. Esto generalmente se puede hacer.</li>
		  			<li>Si no se pueden evitar los par�metros de destino, aseg�rese de que el valor suministrado sea v�lido y
		  				est� autorizado para el usuario. Se recomienda que cualquiera de dichos par�metros de destino sea un valor de mapeo,
		  				en lugar de la URL real o parte de la URL, y ese c�digo del lado del servidor traduce esta asignaci�n a la URL de destino.
		  				Las aplicaciones pueden usar ESAPI para anular el m�todo sendRedirect() para asegurarse de que todos
		  				los destinos de redirecci�n sean seguros.</li>
		  		</ol>
			</p>
			<p>Evitar tales fallas es extremadamente importante ya que son un objetivo favorito de los phishers
				que intentan ganarse la confianza del usuario.</p>
		</div>


</nav>
