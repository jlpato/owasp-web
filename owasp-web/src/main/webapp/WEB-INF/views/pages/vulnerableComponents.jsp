<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    	
    	<h1>A9 - Using Componentes with Known Vulnerabilities</h1>

		<h2><hr/>Usar componentes con vulnerabilidades conocidas.</h2>
		<p>Algunos componentes vulnerables (por ejemplo, bibliotecas de frameworks) se pueden identificar
			y explotar con herramientas automatizadas, expandiendo el grupo de agentes de amenazas
			m�s all� de atacantes espec�ficos para incluir actores ca�ticos.</p>

		<div class="square">
			<h2>Mecanismos preventivos</h2>
		  	<p>
		  		<ol>
		  			<li>Identificar todos los componentes y las versiones que se utilizan en las aplicaciones web
		  				no solo restringidas a bases de datos / frameworks.</li>
		  			<li>Mantenga todos los componentes tales como bases de datos p�blicas,
		  				listas de correo de proyectos, etc. actualizados</li>
		  			<li>Agregar wrappers de seguridad alrededor de componentes que son vulnerables.</li>
		  		</ol>
		  	</p>

		</div>


</nav>
