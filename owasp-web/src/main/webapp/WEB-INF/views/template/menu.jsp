<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style type="text/css">
<!--
	/*
    DEMO STYLE
*/
@import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";


body {
    font-family: 'Poppins', sans-serif;
    background: #fafafa;
}

p {
    font-family: 'Poppins', sans-serif;
    font-size: 1.1em;
    font-weight: 300;
    line-height: 1.7em;
    color: #999;
}

a, a:hover, a:focus {
    color: inherit;
    text-decoration: none;
    transition: all 0.3s;
}

.navbar {
    padding: 15px 10px;
    background: #fff;
    border: none;
    border-radius: 0;
    margin-bottom: 40px;
    box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
}

.navbar-btn {
    box-shadow: none;
    outline: none !important;
    border: none;
}

.line {
    width: 100%;
    height: 1px;
    border-bottom: 1px dashed #ddd;
    margin: 40px 0;
}

/* ---------------------------------------------------
    SIDEBAR STYLE
----------------------------------------------------- */
.wrapper {
    display: flex;
    align-items: stretch;
}

#sidebar {
    /*min-width: 250px;
    max-width: 250px;*/
    min-width: 100%;
    max-width: 100%;
    background: #163c7a;
    /*color: #fff;*/
    color: #7386D5;
    transition: all 0.3s;
}

#sidebar.active {
    margin-left: -250px;
}

#sidebar .sidebar-header {
    padding: 20px;
    background: #101c4d;
}

#sidebar ul.components {
    padding: 20px 0;
    border-bottom: 1px solid #47748b;
}

#sidebar ul p {
    color: #e3e3e3;
    padding: 10px;
}

#sidebar ul li a {
    padding: 10px;
    font-size: 1.1em;
    display: block;
    color: #7386D5;
	background: #101c4d;
}
#sidebar ul li a:hover {
    color: #fff;
    background: #426091;
}

#sidebar ul li.active > a, a[aria-expanded="true"] {
    color: #0C2383;
    background: #bbc6f2;
}


a[data-toggle="collapse"] {
    position: relative;
}

a[aria-expanded="false"]::before, a[aria-expanded="true"]::before {
    content: '\e259';
    display: block;
    position: absolute;
    right: 20px;
    font-family: 'Glyphicons Halflings';
    font-size: 0.6em;
}
a[aria-expanded="true"]::before {
    content: '\e260';
}


ul ul a {
    font-size: 0.9em !important;
    padding-left: 30px !important;
    background: #c8d1f5;
}

ul.CTAs {
    padding: 20px;
}

ul.CTAs a {
    text-align: center;
    font-size: 0.9em !important;
    display: block;
    border-radius: 5px;
    margin-bottom: 5px;
}

a.download {
    background: #333;
    color: #7386D5;
}

a.article, a.article:hover {
    background: #c8d1f5 !important;
    color: #fff !important;
}



/* ---------------------------------------------------
    CONTENT STYLE
----------------------------------------------------- */
#content {
    padding: 20px;
    min-height: 100vh;
    transition: all 0.3s;
}

/* ---------------------------------------------------
    MEDIAQUERIES
----------------------------------------------------- */
@media (max-width: 768px) {
    #sidebar {
        margin-left: -250px;
    }
    #sidebar.active {
        margin-left: 0;
    }
    #sidebarCollapse span {
        display: none;
    }
}
	
-->
</style>


<nav id="sidebar">
    <div class="sidebar-header">
	    <div style="width: 100%;" align="center">
	    <a href="http://praxis.com.mx/">
	    	<img src="${pageContext.request.contextPath}/resources/images/logoPraxis_es.png">
	    </a>
	    </div>
    </div>

    <ul class="list-unstyled components">
        <p>Bienvenido -- ${pageContext.request.userPrincipal.name} --</p>
        <li>
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Home</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                <sec:authorize access="hasRole('ADMIN')">
		            <li><a href="${pageContext.request.contextPath}/admin">Admin</a></li>
		        </sec:authorize>
		       <li><a href="${pageContext.request.contextPath}/products">Products</a></li>
		       <li><a href="${pageContext.request.contextPath}/contactus">Contact Us</a></li>
            </ul>
        </li>
        <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">OWASP 2017</a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
				<li><a href="${pageContext.request.contextPath}/view?v=2013_2017">2013 vs 2017</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017injection">A1 - Injection</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017sessionManagment">A2 - Broken Authentication and Session Managment</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017sensitiveDataExposure">A3 - Sensitive Data Exposure</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017xmlExternalEntities">A4 - XML External Entities (XXE)</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017brokenAccessControl">A5 - Broken Access Control</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017securityMisconfiguration">A6 - Security Misconfiguration</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017crossSiteScripting">A7 - Cross Site Scripting (XSS)</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017insecureDeserialization">A8 - Insecure Deserialization</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017vulnerableComponents">A9 - Using Components with Known Vulnerabilities</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=2017insufficientLoggingMonitoring">A10 - Insufficient Logging and Monitoring</a></li>
		   </ul>
        </li>
		 <li>
            <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false">OWASP 2013</a>
            <ul class="collapse list-unstyled" id="pageSubmenu2">
				<li><a href="${pageContext.request.contextPath}/view?v=injection">A1 - Injection</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=sessionManagment">A2 - Broken Authentication and Session Managment</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=crossSiteScripting">A3 - Cross Site Scripting (XSS)</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=insecureDirectObjectReferences">A4 - Insecure Direct Object References</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=securityMisconfiguration">A5 - Security Misconfiguration</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=sensitiveDataExposure">A6 - Sensitive Data Exposure</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=missingFunctionLevelAccesControl">A7 - Missing Function Level Access Control</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=crossSiteRequestForgery">A8 - Cross-Site Request Forgery (CSRF)</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=vulnerableComponents">A9 - Using Components with Known Vulnerabilities</a></li>
				<li><a href="${pageContext.request.contextPath}/view?v=unvalidatedRedirectsForwards">A10 - Unvalidated Redirects and Forwards</a></li>
		   </ul>
        </li>
        <li>
        	<a href="" onclick="$( '#formOut' ).submit();">Logout</a>
	        <form action="./logout" method="post" id="formOut">
	        	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	       </form>
        </li>
    </ul>

<!--
	<ul class="list-unstyled CTAs">
        <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
        <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li>
    </ul>
 -->
 </nav>
<%-- 
<nav>
    <div style="width: 100%;" align="center">
    <a href="http://praxis.com.mx/">
    	<img src="${pageContext.request.contextPath}/resources/images/logoPraxis_es.png">
    </a>
    </div>
    <br/><br/>
    <div style="width: 100%; color: white;" align="center">
	    Bienvenido -- ${pageContext.request.userPrincipal.name} --
    </div>
    <br/>
    
    <div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          OWASP 2013
        </button>
      </h5>
    </div>

	<ul id="menu">
        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
        <sec:authorize access="hasRole('ADMIN')">
            <li><a href="${pageContext.request.contextPath}/admin">Admin</a></li>
        </sec:authorize>
		<li><a href="${pageContext.request.contextPath}/view?v=injection">A1 - Injection</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=sessionManagment">A2 - Broken Authentication and Session Managment</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=crossSiteScripting">A3 - Cross Site Scripting (XSS)</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=insecureDirectObjectReferences">A4 - Insecure Direct Object References</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=securityMisconfiguration">A5 - Security Misconfiguration</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=sensitiveDataExposure">A6 - Sensitive Data Exposure</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=missingFunctionLevelAccesControl">A7 - Missing Function Level Access Control</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=crossSiteRequestForgery">A8 - Cross-Site Request Forgery (CSRF)</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=vulnerableComponents">A9 - Using Components with Known Vulnerabilities</a></li>
		<li><a href="${pageContext.request.contextPath}/view?v=unvalidatedRedirectsForwards">A10 - Unvalidated Redirects and Forwards</a></li>
       <li><a href="${pageContext.request.contextPath}/products">Products</a></li>
       <li><a href="${pageContext.request.contextPath}/contactus">Contact Us</a></li>
       <li><a href="" onclick="$( '#formOut' ).submit();">Logout</a>
	       <form action="./logout" method="post" id="formOut">
	               <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	               <!-- <input type="submit" value="Salir"/> -->
	       </form>
	       </li>
    </ul>
        
        
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Collapsible Group Item #2
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
</div>
    
    
        <ul id="menu">
       <li><a href="" onclick="$( '#formOut' ).submit();">Logout</a>
	       <form action="./logout" method="post" id="formOut">
	               <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	               <!-- <input type="submit" value="Salir"/> -->
	       </form>
	       </li>
    </ul>
     --%>
    
</nav>