<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
	<div class="container-fluid">
    	
		<div class="square">
		  	<h2>OWASP</h2>
		  	<p>Es un proyecto de c&oacute;digo abierto dedicado a determinar y combatir las causas que hacen que el software sea inseguro. La Fundaci&oacute;n OWASP es un organismo sin &aacute;nimo de lucro que apoya y gestiona los proyectos e infraestructura de OWASP</p>
		</div>
		
		<div class="square">
		  	<h2>Spring Framework</h2>
		  	<p>Es un framework para el desarrollo de aplicaciones y contenedor de inversi&oacute;n de control, de c&oacute;digo abierto para la plataforma Java.</p>
		</div>

		<div class="square">
		  	<h2>Spring Security Framework</h2>
		  	<p>Es un framework de autenticaci&oacute;n y control de acceso potente y altamente personalizable</p>
		  	<p>Es el est&aacute;ndar de facto para asegurar aplicaciones basadas en Spring</p>
		</div>

		<div class="square">
		  	<h2>Hibernate Framework</h2>
		  	<p>Hibernate es una herramienta de mapeo objeto-relacional (ORM) para la plataforma Java que facilita el mapeo de atributos entre una base de datos relacional tradicional y el modelo de objetos de una aplicaci&oacute;n, mediante archivos declarativos (XML) o anotaciones en los beans de las entidades que permiten establecer estas relaciones.</p>
		</div>

		<div class="square">
			<h2>Spring JDBC Template Framework</h2>
			<p>Spring proporciona una simplificaci&0acute;n en el manejo del acceso a base de datos con JBDC Template</p>
			<p>Spring JBDC Template tiene las siguientes ventajas en comparaci&oacute;n con el est&aacute;ndar JDBC</p>
			<ul>
				<li>Spring JBDC Template permite limpiar los recursos autom&aacute;ticamente, por ejemplo, liberar las conexiones a la base de datos.</li>
				<li>Spring JBDC Template convierte las excepciones est&aacute;ndar de <i>SQLException</i> en <i>RuntimeExceptions</i>.
					Esto le permite al programador una reacci&oacute;n mas flexible antes los errores.
					Spring JBDC Template tambi&eacute;n convierte los errores espec&iacute;ficos del proovedor en mensajes de error mas entendibles.</li>
			</ul>
		</div>

    </div>
</nav>
